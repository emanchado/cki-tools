import argparse
import os
import sys
import gitlab
from pipeline_tools import TOOLS, utils
from cki_lib.logger import get_logger
from cki_lib.session import get_session

LOGGER = get_logger('pipeline_tools')
SESSION = get_session('pipeline_tools', LOGGER)


def main():
    parser = argparse.ArgumentParser(description='Pipeline tools')
    parser.add_argument(
        'tool',
        choices=TOOLS.keys(),
        help=('Name of the pipeline tool'),
    )
    parser.add_argument(
        '--project',
        type=str,
        help=(
            'GitLab project, defaults to GITLAB_PROJECT environment variable '
            'or cki-project/cki-pipeline'
        ),
        default=os.getenv('GITLAB_PROJECT', 'cki-project/cki-pipeline'),
    )
    parser.add_argument(
        '-p', '--pipeline-id',
        type=int,
        help='Gitlab pipeline id, required in retrigger and cancel_pipeline',
    )
    parser.add_argument(
        '-b', '--cki-pipeline-branch',
        type=str,
        help='CKI pipeline branch, required in get_last_successful_pipeline)',
    )
    parser.add_argument(
        '--pipeline-definition-repository',
        help='Use a different repository for the cki_pipeline.yml file (e.g. https://gitlab.com/repo)',
    )
    parser.add_argument(
        '--pipeline-definition-branch',
        help='Use a different branch for the cki_pipeline.yml file',
    )
    parser.add_argument(
        '--tree-repository',
        help='Use a different repository for the tree/*.yml file (e.g. https://gitlab.com/repo)',
    )
    parser.add_argument(
        '--tree-branch',
        help='Use a different branch for the tree/*.yml file',
    )
    parser.add_argument(
        '-t', '--cki-pipeline-type',
        type=str,
        help='CKI pipeline type, used in get_last_successful_pipeline',
    )
    parser.add_argument(
        '--tests-only',
        help='Run only testing stage, skipping all the previous stages, '
             'used in retrigger',
        action='store_true',
    )
    parser.add_argument(
        "--variables",
        action=utils.StoreNameValuePair,
        nargs='+',
        help=(
            'Variables to replace in the new pipeline triggered '
            'e.g. --variables mail_to=nobody\nUsed in retrigger'
        ),
        default={},
    )
    parser.add_argument(
        '--variable-filter', action=utils.StoreNameValuePair, default={},
        metavar='KEY=VALUE', help='Filter variables with regular expressions' +
                                  ', used in get_last_successful_pipeline')
    parser.add_argument('-w', '--wait',
                        help='Wait until the pipeline is completed, '
                             'used in retrigger',
                        action='store_true')
    parser.add_argument(
        '--trigger-token',
        default=os.getenv('GITLAB_TRIGGER_TOKEN'),
        help=(
            'GitLab trigger token, defaults to GITLAB_TRIGGER_TOKEN '
            'environment variable'
        )
    )
    args = parser.parse_args()

    if (args.tool == 'cancel_pipeline' or args.tool == 'retrigger') and \
       args.pipeline_id is None:
        raise Exception("Pipeline_id argument is required in cancel_pipeline")

    if args.tool == 'get_last_successful_pipeline' and \
       args.cki_pipeline_branch is None:
        raise Exception("Cki pipeline branch argument is required in "
                        "get_last_successful_pipeline")

    gitlab_url = utils.get_env_var_or_raise('GITLAB_URL')
    private_token = utils.get_env_var_or_raise('GITLAB_PRIVATE_TOKEN')

    gitlab_instance = gitlab.Gitlab(gitlab_url,
                                    private_token=private_token,
                                    api_version=str(4), session=SESSION)

    project = gitlab_instance.projects.get(args.project)

    func = TOOLS[args.tool].main
    if args.tool == 'retrigger':
        if args.trigger_token is None:
            raise Exception('Trigger token needs to be specified with '
                            '--trigger-token or GITLAB_TRIGGER_TOKEN')
        status = func(project, args)
        sys.exit(status)
    elif args.tool == 'cancel_pipeline':
        pipeline = project.pipelines.get(args.pipeline_id)
        func(project, pipeline)
    elif args.tool == 'get_last_successful_pipeline':
        print(func(project, args))
    else:
        raise NotImplementedError


if __name__ == '__main__':
    main()
