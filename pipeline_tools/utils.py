"""Generic functions and constants."""
import argparse
import copy
import logging
import os
import re
import time
from distutils.util import strtobool  # re-exported!

from cki_lib import misc
from cki_lib.retrying import retrying_on_exception


class StoreNameValuePair(argparse.Action):
    """Parse key=value arguments."""

    def __call__(self, parser, namespace, values, option_string=None):
        variables = getattr(namespace, self.dest) or {}
        if isinstance(values, str):
            values = [values]
        for key_value_pair in values:
            key, value = key_value_pair.split('=', 1)
            variables[key] = value
        setattr(namespace, self.dest, variables)


class EnvVarNotSetError(Exception):
    """Requested environment variable is not set."""


def booltostr(b):
    """Convert a boolean to True/False strings."""
    return str(bool(b))


def get_env_var_or_raise(name):
    """Get environment variable and raise EnvVarNotSetError if not set.

    Args:
        name:      Name of the variable's value to retrieve.

    Returns:
        Value of the environment variable, if set.

    Raises:
        EnvVarNotSetError if the variable is not set.
    """
    env_var = os.getenv(name)
    if not env_var:
        raise EnvVarNotSetError(f'Environment variable {name} is not set!')
    return env_var


# This is kept so semantics is the same. However, all Gitlab objects are
# already wrapped in a session to make retries.
@retrying_on_exception(Exception)
def get_variables(pipeline):
    """
    Get the variables used by the corresponding pipeline.

    Args:
        pipeline: Object representing the pipeline to retrieve variables for

    Returns:
        A dictionary representing pipelines's variables.
    """
    return {v.key: v.value for v in pipeline.variables.list(as_list=False)}


def clean_repository_url(repository):
    """Remove any trailing slashes or .git at the end of repository urls.

    The repository URL is used (1) to refer to raw file contents for the GitLab
    CI YAML includes, and (2) inside of the pipeline code to clone the repo.
    Make sure the URL works for both cases even if the URLs passed on the
    command line would not.
    """
    return re.sub(r'(.git)?/*$', '', repository)


def create_commit(project, trigger, title, pipeline_branch=None):
    """Create a commit in the branch where the pipeline will run.

    It's just used for getting an intuitive view using the gitlab interface.

    Args:
        project:         GitLab project to create commit in.
        trigger:         Dictionary with all data to include in the message.
        title:           String to use as the commit message title.
        pipeline_branch: Use this branch instead of cki_pipeline_branch
    """
    commit_message = title + '\n'
    for key, value in trigger.items():
        commit_message += '\n{} = {}'.format(key, value)
    data = {
        'branch': pipeline_branch or trigger['cki_pipeline_branch'],
        'commit_message': commit_message,
        'actions': [],
    }
    if 'tree_name' in trigger and 'PIPELINE_DEFINITION_URL' in os.environ:
        tree_name = trigger['tree_name']
        pipeline_definition_url = clean_repository_url(
            get_env_var_or_raise('PIPELINE_DEFINITION_URL'))
        try:
            project.branches.get(data['branch'])
        except Exception:
            data['start_branch'] = 'master'
            data['actions'].append({
                'action': 'create',
                'file_path': '.gitlab-ci.yml',
                'content': 'include:\n' +
                f'- {pipeline_definition_url}/raw/master/cki_pipeline.yml\n' +
                f'- {pipeline_definition_url}/raw/master/trees/{tree_name}.yml\n'
            })
    project.commits.create(data)


def get_last_successful_pipeline(project, ref, cki_pipeline_type=None,
                                 variable_filter=None):
    """Return the ID of the last successful pipeline of a certain branch.

    The variable_filter parameter can contain regular expressions to only
    return matching pipelines according to re.fullmatch(). The
    cki_pipeline_type parameter is deprecated.

    Returns None if no such pipeline exists.
    """
    list_filter = {"scope": "finished", "status": "success"}
    variable_filter = variable_filter or {}
    if cki_pipeline_type:
        variable_filter['cki_pipeline_type'] = cki_pipeline_type
    return get_last_pipeline(project, ref,
                             list_filter=list_filter,
                             variable_filter=variable_filter)


def get_last_pipeline(project, ref, cki_pipeline_type=None,
                      list_filter=None, variable_filter=None):
    """Return the ID of the last pipeline of a certain branch.

    The list_filter parameter is passed to project.pipelines.list.
    The variable_filter parameter can contain regular expressions to only
    return matching pipelines according to re.fullmatch().
    The cki_pipeline_type parameter is deprecated.

    Returns None if no such pipeline exists.
    """
    variable_filter = variable_filter or {}
    if cki_pipeline_type:
        variable_filter['cki_pipeline_type'] = cki_pipeline_type

    pipelines = project.pipelines.list(as_list=False, ref=ref,
                                       **list_filter or {})

    for pipeline in pipelines:
        variables = get_variables(pipeline)
        if misc.strtobool(variables.get('retrigger', 'False')):
            continue
        if not all(key in variables and re.fullmatch(value, variables[key])
                   for key, value in variable_filter.items()):
            continue
        return pipeline.id

    return None


def trigger_pipeline(project, trigger_token, trigger, pipeline_branch=None):
    """Trigger a pipeline.

    Args:
        project:         gitlab.Gitlab.project object representing the project
        trigger_token:   Token to trigger the pipeline with.
        trigger:         Dictionary containing pipeline variables.
    """

    if 'cki_pipeline_type' not in trigger:
        raise KeyError('cki_pipeline_type is required')
    create_commit(project, trigger, trigger['title'], pipeline_branch)
    pipeline = project.trigger_pipeline(
        pipeline_branch or trigger['cki_pipeline_branch'],
        trigger_token,
        trigger)
    logging.info('Pipeline "%s" triggered', trigger['title'])
    return pipeline


def ensure_cki_project_variable(triggers):
    """
    Convert cki_pipeline_project to cki_project variable.

    Does nothing if a cki_pipeline_project variable does not exist.

    The value of the cki_project variable is the concatenation of the
    GITLAB_PARENT_PROJECT environment variable and the cki_pipeline_project
    trigger variable.
    """
    if 'cki_pipeline_project' not in triggers:
        return

    parent_project = get_env_var_or_raise('GITLAB_PARENT_PROJECT')
    pipeline_project = triggers.pop('cki_pipeline_project')
    triggers['cki_project'] = f'{parent_project}/{pipeline_project}'


def safe_trigger(gitlab_instance, trigger_token, triggers, variables=None):
    """
    Trigger production or testing pipelines, based on whether IS_PRODUCTION is
    set or not. Production variables are removed for testing pipelines.

    Args:
        gitlab_instance: gitlab.Gitlab object reporesenting GitLab instance.
        trigger_token:   Token to trigger the pipeline with.
        triggers:        List of dictionaries describing the pipeline to
                         trigger. The dictionary contains pipeline variables.

    Returns: A list of triggered pipelines if no errors occurred.
    Raises: First exception that happened when triggering the pipelines.
    """
    errors = []
    triggered_pipelines = []

    for index, trigger in enumerate(triggers):
        try:
            trigger.update(variables or {})
            ensure_cki_project_variable(trigger)
            if not misc.is_production():
                clean_production_vars(trigger)
                time.sleep(30)  # try to avoid collision with production deployment

            project = gitlab_instance.projects.get(trigger['cki_project'])

            # Make sure only strings are passed to make GitLab happy
            for key, value in trigger.items():
                if not isinstance(value, str):
                    trigger[key] = str(value)

            triggered_pipelines.append(
                trigger_pipeline(project, trigger_token, trigger)
            )

            logging.info('Pipeline %d/%d for %s triggered',
                         index + 1,
                         len(triggers),
                         trigger['cki_pipeline_branch'])
        except Exception as e:
            errors.append(e)
            logging.exception('Pipeline %d/%d for %s could not be triggered',
                              index + 1,
                              len(triggers),
                              trigger['cki_pipeline_branch'])
    if errors:
        raise errors[0]

    return triggered_pipelines


def wait_for_pipeline(pipeline):
    time.sleep(30)
    pipeline.refresh()
    while pipeline.attributes['status'] in ['running', 'pending']:
        time.sleep(60)
        print('.', end='')
        pipeline.refresh()
    print()
    return pipeline.attributes['status'] == 'success'


# pylint: disable=too-many-arguments
def _reset_variable(variables, new_variables, name, message,
                    new_value=None, force=False):
    """Unless overridden on the command line, reset a variable."""
    if (name in variables or force) and name not in (new_variables or {}):
        logging.warning(message)
        if new_value is not None:
            variables[name] = new_value
        elif name in variables:
            del variables[name]


def clean_production_vars_notifications(variables, new_variables=None):
    """Sanitize variables related to email notifications.

    Ideally, emails will not be sent for pipelines marked with retrigger: true.
    At the moment, this is not true for rhcheckpatch, so that job is disabled.

    Just to be sure, remove all trigger variables with email addresses.
    """
    _reset_variable(variables, new_variables, 'send_report_to_upstream',
                    'Not sending email to upstream', 'false')
    _reset_variable(variables, new_variables, 'skip_rhcheckpatch',
                    'Disabling rhcheckpatch', 'true', force=True)
    _reset_variable(variables, new_variables, 'mail_to',
                    'Not sending email as mail_to variable is not set')
    _reset_variable(variables, new_variables, 'mail_cc',
                    'Not sending cc email as mail_cc variable is not set')
    _reset_variable(variables, new_variables, 'mail_bcc',
                    'Not sending bcc email as mail_bcc variable is not set')
    _reset_variable(variables, new_variables, 'mail_add_maintainers_to',
                    'Disabling mailing maintainers')


def clean_production_vars_retrigger(variables, new_variables=None):
    """Sanitize variables related to the pipeline type + mark as retrigger.

    Mark pipelines as non-production by adding retrigger: true. Make that obvious
    by adding 'Retrigger' to the pipeline commit message ("title").
    Ideally, those pipelines will be skipped for anything where a production pipeline
    is required.

    Just to be sure, additionally mark the pipelines by modifying the email subject
    and pipeline type."""
    _reset_variable(variables, new_variables, 'cki_pipeline_type',
                    'Prepending `retrigger-` to cki_pipeline_type ',
                    f'retrigger-{variables["cki_pipeline_type"]}')
    subject = variables.get('subject') or variables.get('title')
    subject = subject or "Empty subject"
    variables['subject'] = f'Retrigger: {subject}'
    variables['title'] = f'Retrigger: {variables["cki_pipeline_type"]}'
    variables['retrigger'] = 'true'


def clean_production_vars_tracking(variables, new_variables=None):
    """Sanitize variables related to last-patch tracking.

    The patchwork triggers add variables to production pipelines to keep their state.
    Remove those variables as otherwise the state of those triggers would be reset to
    the moment the original pipelines were triggered."""
    if variables.get('cki_pipeline_type') == 'patchwork-v1':
        _reset_variable(variables, new_variables, 'last_patch_id',
                        'Disabling Patchwork V1 last-patch tracking')
    if variables.get('cki_pipeline_type') == 'patchwork':
        _reset_variable(variables, new_variables, 'event_id',
                        'Disabling Patchwork last-patch tracking')


def clean_production_vars_ystream(variables, new_variables=None):
    """Sanitize variables related to ystream compose selection.

    Pipelines for RHEL y-streams run on a nightly compose that will disappear
    after a while. The compose is normally explicitly specified in
    pipeline-data in the ystream_distro variable so that a pipeline is
    reproducible. By removing this variable, the latest accepted compose is
    used, which should be good enough for retriggered pipelines."""
    _reset_variable(variables, new_variables, 'ystream_distro',
                    'Disabling generic ystream distro name')


def clean_production_vars_beaker(variables, new_variables=None):
    """Sanitize variables related to beaker testing.

    Production pipelines are tested in Beaker. As retriggered pipelines are
    mostly for CI purposes, skip Beaker by default to preserve testing resources."""
    _reset_variable(variables, new_variables, 'skip_beaker',
                    'Disabling beaker testing', 'true', force=True)


def clean_production_vars(variables, new_variables=None):
    """Sanitize all variables."""
    clean_production_vars_tracking(variables, new_variables)
    clean_production_vars_notifications(variables, new_variables)
    clean_production_vars_ystream(variables, new_variables)
    clean_production_vars_beaker(variables, new_variables)
    clean_production_vars_retrigger(variables, new_variables)


def merge_dicts(config, new):
    """Recursively merge values from new into config dictionary."""
    for key, value in new.items():
        if (key in config and
                isinstance(value, dict) and isinstance(config[key], dict)):
            merge_dicts(config[key], value)
        else:
            config[key] = copy.deepcopy(value)
    return config


def _merged(configs, key, use_default=False):
    if use_default and '.default' in configs:
        config = _merged(configs, '.default')
    else:
        config = {}
    extends = configs[key].get('.extends', [])
    if isinstance(extends, str):
        extends = [extends]
    for extend in extends:
        merge_dicts(config, _merged(configs, extend))
    merge_dicts(config, configs[key])
    config.pop('.extends', None)
    return config


def process_config_tree(configs):
    """Remove configurations starting with a dot, and process .extends.

    This is similar to the `extends` functionality of GitLab .gitlab-ci.yml
    files. For a dict of configuration dicts, a member `.extends` in a
    configuration dict can specify the name or list of names of other
    configuration dicts to inherit from.

    A `.default` configuration will be used for inheritance even if .extends is
    not specified.

    All configurations with a name starting with a dot will be removed.
    """
    return {k: _merged(configs, k, True) for k in configs.keys()
            if not k.startswith('.')}


def clean_config(value):
    """Remove dict members starting with a dot."""
    if isinstance(value, dict):
        return {k: clean_config(v) for k, v in value.items()
                if not k.startswith('.')}
    if isinstance(value, list):
        return [clean_config(v) for v in value]
    return value
