"""Trigger pipelines from brew/koji."""
import argparse
from datetime import datetime
import os
import pathlib
import re

import koji
import requests
import yaml
from gitlab import Gitlab

from cki_lib import logger
from cki_lib import misc
from cki_lib import session

from . import utils

LOGGER = logger.get_logger('cki.pipeline_tools.brew_trigger')
SESSION = session.get_session('cki.pipeline_tools.brew_trigger', LOGGER)


def get_nvr(session, task_data):
    """Try to get the NVR from the task tree."""
    task_id = task_data['id']
    request_string = task_data['request'][0]
    rpm_match = re.search(r'/rpms/(.*)(?:.git)?\??#', request_string)
    # Handle official builds from dist-git
    if rpm_match:
        LOGGER.info('%s: Build from dist-git found!', task_id)
        # These don't contain full NVR in the request, just "kernel", and
        # we need to get it from the task itself.
        build_info = session.listBuilds(taskID=task_id)
        if build_info:
            return build_info[0]['nvr']

    # Handle scratch builds from SRPM
    nvr_match = re.search(r'.*/(.*\.rpm)', request_string)
    if nvr_match:
        return nvr_match.group(1)

    # Handle all the rest -- scratch builds from dist git or git trees
    for child in session.listTasks(
            opts={'parent': task_id, 'method': 'buildArch', 'decode': True},
            queryOpts={'limit': 1}):
        return os.path.basename(child['request'][0])

    LOGGER.error('%s: Can\'t get NVR from task!', task_id)
    return ''


def get_brew_trigger_variables(task_id, brew_config, server_section):
    """Determine the trigger variables for a brew task."""
    server_url = brew_config[server_section]['server_url']
    session = koji.ClientSession(server_url)

    task_data = session.getTaskInfo(task_id, request=True)

    if not task_data.get('request'):
        LOGGER.info('%s: Task doesn\'t have a build request, ignoring',
                    task_id)
        return None
    task_options = task_data['request'][2]

    nvr = get_nvr(session, task_data)
    if not nvr:
        return None

    LOGGER.info('%s: Build %s found!', task_id, nvr)

    for pipeline_name, pipeline_config in \
            utils.process_config_tree(brew_config).items():
        if 'result_pipe' in pipeline_config:
            continue
        if not re.search(r'^{}-\d+[.-](\S+[.-])+{}'.format(
                pipeline_config['package_name'],
                pipeline_config['rpm_release']), nvr):
            continue

        variables = pipeline_config.copy()

        LOGGER.info('%s: Found possible trigger', task_id)
        if task_options.get('scratch') and \
                not utils.strtobool(variables.get('.test_scratch', 'False')):
            LOGGER.info('%s: Scratch build testing disabled in trigger',
                        task_id)
            continue

        if 'owner' in task_data:
            owner_name = session.getUser(task_data['owner'])['name']
            configured_owners = variables.get('.owners', [])
            if not configured_owners or owner_name in configured_owners:
                variables['owner'] = owner_name
            else:
                LOGGER.info('%s: Trigger not configured for user %s',
                            task_id, owner_name)
                continue

        variables['is_scratch'] = utils.booltostr(task_options.get('scratch'))

        brew_arches = task_options.get('arch_override')
        if brew_arches:
            if 'arch_override' in variables:
                # Override brew's override by user's, but only for arches that
                # were actually built. Split out the code to multiple lines so
                # we actually understand what's going on...
                brew_arch_set = set(brew_arches.split())
                user_arch_set = set(variables['arch_override'].split())
                new_arches = user_arch_set & brew_arch_set
                variables['arch_override'] = ' '.join(new_arches)
            else:
                variables['arch_override'] = brew_arches

        variables['nvr'] = nvr
        variables['brew_task_id'] = str(task_id)
        variables['title'] = f'Brew: Task {task_id}'
        variables['cki_pipeline_type'] = 'brew'
        variables['name'] = pipeline_name
        variables['discovery_time'] = f'{datetime.utcnow().isoformat()}Z'

        return utils.clean_config(variables)

    # We iterated over all triggers and didn't find a match
    LOGGER.info('%s: Pipeline for %s not configured!', task_id, nvr)
    return None


def main():
    """Command line interface to the triggering brew pipelines."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--gitlab-url', default=os.environ.get('GITLAB_URL'),
        help='GitLab URL (default: value of GITLAB_URL environment variable)')
    parser.add_argument(
        '--private-token', default='GITLAB_PRIVATE_TOKEN',
        help='Name of the env variable that contains the GitLab private token'
        ' (default: GITLAB_PRIVATE_TOKEN)')
    parser.add_argument(
        '--trigger-token', default='GITLAB_TRIGGER_TOKEN',
        help='Name of the env variable that contains the GitLab trigger token'
        ' (default: GITLAB_TRIGGER_TOKEN)')
    config = parser.add_mutually_exclusive_group()
    config.add_argument(
        '-c', '--config', default='brew.yaml',
        help='YAML configuration file to use (default: brew.yaml)')
    config.add_argument(
        '--config-url',
        help='URL to YAML configuration file to use.')
    parser.add_argument(
        '--server-section', default='.amqp',
        help='YAML section with server configuration (default: .amqp)')
    parser.add_argument(
        "--variables", action=utils.StoreNameValuePair, default={},
        metavar='KEY=VALUE', help='Override trigger variables')
    parser.add_argument(
        'task_id',
        help='Brew task ID')
    args = parser.parse_args()

    if args.config_url:
        config_content = requests.get(args.config_url).content
    else:
        config_content = pathlib.Path(args.config).read_text()

    config = yaml.safe_load(config_content)
    variables = get_brew_trigger_variables(args.task_id, config,
                                           args.server_section)
    variables.update(args.variables)
    if not misc.is_production():
        utils.clean_production_vars(variables, args.variables)
    print('Trigger variables:')
    for key, value in variables.items():
        print(f'  {key}: {value}')
    if misc.is_production():
        if input('Are you sure that you want to submit a new pipeline with '
                 'these variables (enter upper case yes)? ') != 'YES':
            raise Exception('Aborting...')

    instance = Gitlab(args.gitlab_url,
                      private_token=os.getenv(args.private_token),
                      session=SESSION)
    project = instance.projects.get(variables['cki_project'])
    pipeline = utils.trigger_pipeline(project, os.getenv(args.trigger_token),
                                      variables)
    print(f'Pipeline: {pipeline.web_url}')


if __name__ == '__main__':
    main()
