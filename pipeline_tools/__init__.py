import logging
import sys
from cki_lib.logger import FORMAT
from . import cancel_pipeline
from . import get_last_successful_pipeline
from . import retrigger

logging.basicConfig(format=FORMAT, level=logging.DEBUG)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)

TOOLS = {
    'cancel_pipeline': cancel_pipeline,
    'get_last_successful_pipeline': get_last_successful_pipeline,
    'retrigger': retrigger
}
