"""Tests for triggers.utils."""
import argparse
import os
import unittest.mock

from pipeline_tools import utils

import fakes


@unittest.mock.patch('pipeline_tools.utils.time.sleep', unittest.mock.Mock())
class TestTrigger(unittest.TestCase):
    """Tests for utils.safe_trigger()."""

    required_variables = {'cki_project': 'cki-project',
                          'cki_pipeline_branch': 'test_branch',
                          'cki_pipeline_type': 'baseline'}
    required_variables_env = {'cki_pipeline_project': 'cki-pipeline',
                              'cki_pipeline_branch': 'test_branch',
                              'cki_pipeline_type': 'baseline'}
    multiple_variables = [{
        'cki_project': 'cki-project',
        'cki_pipeline_branch': 'branch 1',
        'title': 'title 1',
        'cki_pipeline_type': 'baseline',
    }, {
        'cki_project': 'cki-project',
        'cki_pipeline_branch': 'branch 2',
        'title': 'title 2',
        # 'cki_pipeline_type' missing
    }, {
        'cki_project': 'cki-project',
        'cki_pipeline_branch': 'branch 3',
        'title': 'title 3',
        'cki_pipeline_type': 'baseline',
    }]

    def _trigger_single(self, variables, project_name):

        gitlab = fakes.FakeGitLab()
        gitlab.add_project(project_name)

        utils.safe_trigger(gitlab, 'token', [variables])

        pipeline = gitlab.projects[project_name].pipelines[0]
        for key, value in variables.items():
            self.assertEqual(pipeline.attributes[key], value)
        self.assertEqual(pipeline.attributes['cki_project'], project_name)

    def test_trigger_single(self):
        """
        Test triggering a single pipeline. Only add required variables to make
        sure the code doesn't suddenly change to require something else.
        """
        self._trigger_single(self.required_variables, 'cki-project')

    @unittest.mock.patch.dict(os.environ, {'GITLAB_PARENT_PROJECT': 'cki-project'})
    def test_trigger_single_env(self):
        """
        Test triggering a single pipeline. Only add required variables to make
        sure the code doesn't suddenly change to require something else.
        """
        self._trigger_single(self.required_variables_env, 'cki-project/cki-pipeline')

    def _check_required_variables(self, variables, project_name, errors):
        gitlab = fakes.FakeGitLab()
        gitlab.add_project(project_name)

        for expected in variables:
            missing = variables.copy()
            del missing[expected]
            with self.assertRaises(errors):
                utils.safe_trigger(gitlab, 'token', [missing])

    def test_required_variables(self):
        """
        Verify the expected variables are still required to trigger pipelines
        successfully.
        """
        self._check_required_variables(self.required_variables,
                                       'cki-project',
                                       (KeyError, utils.EnvVarNotSetError))

    @unittest.mock.patch.dict(os.environ, {'GITLAB_PARENT_PROJECT': 'cki-project'})
    def test_required_variables_env(self):
        """
        Verify the expected variables are still required to trigger pipelines
        successfully.
        """
        self._check_required_variables(self.required_variables_env,
                                       'cki-project/cki-pipeline',
                                       KeyError)

    def test_trigger_multiple_errors(self):
        """
        Test triggering multiple pipelines, some of which invalid. All
        pipelines should be tried.
        """
        variables = self.multiple_variables

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('cki-project')

        with self.assertRaises(Exception):
            utils.safe_trigger(gitlab, 'token', variables)

        pipelines = gitlab.projects['cki-project'].pipelines.list()
        self.assertEqual(len(pipelines), 2)
        self.assertEqual(pipelines[0].attributes['title'],
                         variables[0]['title'])
        self.assertEqual(pipelines[1].attributes['title'],
                         variables[2]['title'])

    def test_returned_no_errors(self):
        """
        Verify the function returns a list of triggered pipelines if no erorrs
        were encountered when triggering.
        """
        gitlab = fakes.FakeGitLab()
        gitlab.add_project('cki-project')

        triggers = self.multiple_variables.copy()
        del triggers[1]  # Remove the trigger that's causing exception

        triggered_pipelines = utils.safe_trigger(gitlab, 'token', triggers)
        self.assertEqual(len(triggered_pipelines), 2)


@unittest.mock.patch.dict(os.environ, {'IS_PRODUCTION': 'true'})
class TestTriggerProduction(TestTrigger):
    """Tests for utils.safe_trigger() with IS_PRODUCTION=true."""
    required_variables = TestTrigger.required_variables.copy()
    required_variables.update({'title': 'commit_title'})
    required_variables_env = TestTrigger.required_variables_env.copy()
    required_variables_env.update({'title': 'commit_title'})


class TestCreateCommit(unittest.TestCase):
    """Test cases for utils.create_commit()."""

    def test_created_data(self):
        """Verify the content of created commit looks as expected."""

        commit_text = 'title\n\ncki_pipeline_branch = branch'
        expected_data = {'branch': 'branch',
                         'actions': [],
                         'commit_message': commit_text}

        project = fakes.FakeGitLabProject()
        utils.create_commit(project,
                            {'cki_pipeline_branch': 'branch'},
                            'title')

        self.assertEqual(project.commits[0], expected_data)


class TestArguments(unittest.TestCase):
    """Test cases for argument parsing."""

    def test_store_name_value_pair(self):
        """Verify key=value argument parsing."""
        parser = argparse.ArgumentParser()
        parser.add_argument('--vars', action=utils.StoreNameValuePair,
                            default={})
        args = parser.parse_args('--vars key=value --vars key2=value2'.split())
        self.assertEqual(args.vars, {'key': 'value', 'key2': 'value2'})

    def test_store_name_value_pair_nargs(self):
        """Verify key=value argument parsing with nargs=+."""
        parser = argparse.ArgumentParser()
        parser.add_argument('--vars', action=utils.StoreNameValuePair,
                            nargs='+', default={})
        args = parser.parse_args('--vars key=value key2=value2'.split())
        self.assertEqual(args.vars, {'key': 'value', 'key2': 'value2'})

    def test_store_name_value_pair_equal(self):
        """Verify key=value argument parsing with equal signs."""
        parser = argparse.ArgumentParser()
        parser.add_argument('--vars', action=utils.StoreNameValuePair,
                            default={})
        args = parser.parse_args('--vars key=value=value2'.split())
        self.assertEqual(args.vars, {'key': 'value=value2'})

    def test_store_name_value_pair_no_default(self):
        """Verify key=value argument parsing without default."""
        parser = argparse.ArgumentParser()
        parser.add_argument('--vars', action=utils.StoreNameValuePair)
        args = parser.parse_args('--vars key=value'.split())
        self.assertEqual(args.vars, {'key': 'value'})


class TestProcessConfigTree(unittest.TestCase):
    """Tests for patch_trigger.process_config_tree()."""

    def test_removal(self):
        """Test removal of keys with a leading dot."""
        config = {
            'one': {'two': 'three'},
            '.four': {'five': 'six'},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'two': 'three'}})

    def test_extends(self):
        """Test extending one level deep."""
        config = {
            'one': {'.extends': '.four', 'two': 'three'},
            '.four': {'five': 'six'},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'two': 'three', 'five': 'six'}})

    def test_extends_multilevel(self):
        """Test extending multiple levels deep."""
        config = {
            'one': {'.extends': '.two'},
            '.two': {'.extends': '.three'},
            '.three': {'.extends': '.four'},
            '.four': {'five': 'six'},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'five': 'six'}})

    def test_extends_mixins(self):
        """Test extending from multiple base configs."""
        config = {
            'one': {'.extends': ['.two', '.three', '.four']},
            '.two': {'two': 'five'},
            '.three': {'two': 'six'},
            '.four': {'three': 'seven'},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'two': 'six', 'three': 'seven'}})

    def test_extends_non_dot(self):
        """Test extending one level deep from a non-dot config."""
        config = {
            'one': {'.extends': 'four', 'two': 'three'},
            'four': {'five': 'six'},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'two': 'three', 'five': 'six'}, 'four': {'five': 'six'}})

    def test_extends_override(self):
        """Test extending one level deep with overriding."""
        config = {
            'one': {'.extends': '.four', 'two': 'three'},
            '.four': {'two': 'six', 'five': 'seven'},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'two': 'three', 'five': 'seven'}})

    def test_extends_error(self):
        """Test extending a non-existent base config."""
        config = {
            'one': {'.extends': '.four'},
        }
        self.assertRaises(Exception, lambda: utils.process_config_tree(config))

    def test_extends_default(self):
        """Test inheriting from .default."""
        config = {
            '.default': {'seven': 'eight'},
            'one': {'two': 'three'},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'two': 'three', 'seven': 'eight'}})

    def test_extends_default_extends(self):
        """Test inheriting from .default with a .extends clause."""
        config = {
            '.default': {'seven': 'eight'},
            'one': {'.extends': '.four', 'two': 'three'},
            '.four': {'five': 'six'},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'two': 'three', 'five': 'six', 'seven': 'eight'}})

    def test_extends_default_override(self):
        """Test inheriting from .default with overriding."""
        config = {
            '.default': {'two': 'seven'},
            'one': {'.extends': '.four', 'two': 'three'},
            '.four': {'five': 'six'},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'two': 'three', 'five': 'six'}})

    def test_extends_default_override_indirect(self):
        """Test inheriting from .default with indirect overriding."""
        config = {
            '.default': {'five': 'seven'},
            'one': {'.extends': '.four', 'two': 'three'},
            '.four': {'five': 'six'},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'two': 'three', 'five': 'six'}})

    def test_extends_default_not_extends(self):
        """Test that .default is not used during .extends."""
        config = {
            '.default': {'one': 'two'},
            'one': {'.extends': ['.three', '.four']},
            '.three': {'one': 'five'},
            '.four': {'six': 'seven'},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'one': 'five', 'six': 'seven'}})

    def test_extends_default_extends_default(self):
        """Test that .default can use .extends."""
        config = {
            '.default': {'.extends': '.three', 'two': 'one'},
            'one': {'.extends': ['.four']},
            '.three': {'one': 'five'},
            '.four': {'six': 'seven'},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'one': 'five', 'two': 'one', 'six': 'seven'}})

    def test_clean(self):
        """Test that .stuff is removed during cleaning."""
        config = {'one': 'two', '.two': 'three', 'four': [{'.five': 'six'}]}
        self.assertEqual(utils.clean_config(config),
                         {'one': 'two', 'four': [{}]})

    def test_merge_deep(self):
        """Test that dict merging works at the deeper levels."""
        config = {
            'one': {'.extends': ['.two'], 'three': {'four': {'five': 'six'}}},
            '.two': {'three': {'four': {'seven': 'eight'}}},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'three': {'four': {'five': 'six', 'seven': 'eight'}}}})

    def test_merge_override(self):
        """Test that dict merging overrides keys."""
        config = {
            'one': {'.extends': ['.two'], 'three': {'four': {'five': 'six'}}},
            '.two': {'three': {'four': {'five': 'seven'}}},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'three': {'four': {'five': 'six'}}}})

    def test_merge_one_level(self):
        """Test that dict merging works at the top level."""
        config = {
            'one': {'.extends': ['.two'], 'three': {'four': 'five'}},
            '.two': {'three': {'six': 'seven'}},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'three': {'four': 'five', 'six': 'seven'}}})

    def test_merge_array(self):
        """Test that merging overrides arrays."""
        config = {
            'one': {'.extends': ['.two'], 'three': ['four', 'five']},
            '.two': {'three': ['six', 'seven']},
        }
        self.assertEqual(utils.process_config_tree(config), {
            'one': {'three': ['four', 'five']}})

    def test_merge_dicts(self):
        """Test that dictionaries are correcly merged."""
        config = {'one': 'two', 'two': {'three': 'five'}}
        new = {'one': 'four', 'two': {'five': 'six'}, 'three': 'six'}
        self.assertEqual(utils.merge_dicts(config, new), {
            'one': 'four', 'three': 'six',
            'two': {'three': 'five', 'five': 'six'}})


class TestBoolStr(unittest.TestCase):
    """Test cases for booltostr and strtobool."""

    def test_bool_str(self):
        """Check conversion for booltostr."""
        self.assertEqual(utils.booltostr(False), 'False')
        self.assertEqual(utils.booltostr(None), 'False')
        self.assertEqual(utils.booltostr(''), 'False')
        self.assertEqual(utils.booltostr([]), 'False')
        self.assertEqual(utils.booltostr(True), 'True')
        self.assertEqual(utils.booltostr('str'), 'True')
        self.assertEqual(utils.booltostr(['array']), 'True')

    def test_str_bool(self):
        """Check conversion for strtobool."""
        self.assertEqual(utils.strtobool('False'), False)
        self.assertEqual(utils.strtobool('false'), False)
        self.assertEqual(utils.strtobool('True'), True)
        self.assertEqual(utils.strtobool('true'), True)


class TestGetLastPipeline(unittest.TestCase):
    """
    Test cases for utils.get_last_pipeline()
    and utils.get_last_successful_pipeline().
    """

    def test_get_last_pipeline(self):
        """Verify the correct pipelines are returned with different filters."""

        project = fakes.FakeGitLabProject()

        branch = 'branch'
        cki_pipeline_type = 'baseline'
        scope = 'finished'
        variables = {'cki_pipeline_type': cki_pipeline_type, 'scope': scope}

        project.pipelines.add_new_pipeline(branch, 'token',
                                           variables, 'success')
        last_successful_pipeline = project.pipelines.list(ref=branch,
                                                          status='success')[0]

        project.pipelines.add_new_pipeline(branch, 'token',
                                           variables, 'failed')
        last_pipeline = project.pipelines.list(ref=branch,
                                               status='failed')[0]

        self.assertEqual(utils.get_last_pipeline(
            project, branch,
            variable_filter={'cki_pipeline_type': cki_pipeline_type},
            list_filter={"scope": scope}
        ), last_pipeline.id)
        self.assertEqual(utils.get_last_successful_pipeline(
            project, branch,
            variable_filter={'cki_pipeline_type': cki_pipeline_type},
        ), last_successful_pipeline.id)
