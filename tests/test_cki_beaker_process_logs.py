import os
import pathlib
import tempfile
import unittest
import xml.etree.ElementTree as ET
from unittest.mock import Mock, patch

from cki.beaker_tools import process_logs

BEAKER_XML = """
<job>
<whiteboard>cki@gitlab:550903 3.10.0-1137.el7.cki@rhel7 x86_64</whiteboard>
<recipeSet>
<recipe arch="x86_64" system="system.beaker.redhat.com">
  <task id="109622839">
  </task>
  <task id="109622840">
    <params>
      <param name="VMCOREPATH" value="/var/www/html/vmcore"/>
      <param name="WEBSERVERBASEURL" value="http://server/vmcore"/>
    </params>
  </task>
  <task id="109622841" result="Pass" status="Completed"
   start_time="2020-05-15 16:52:04" finish_time="2020-05-15 16:56:04" duration="0:04:00">
    <params>
      <param name="CKI_NAME" value="Boot test"/>
      <param name="CKI_UNIVERSAL_ID" value="boot"/>
      <param name="KPKG_URL" value=""/>
    </params>
    <results>
      <result path="dio">None</result>
    </results>
  </task>
  <task id="109622842" result="Pass" status="Completed" start_time="2020-05-15 16:52:04"
   finish_time="2020-05-15 16:57:04" duration="0:05:00">
    <params>
      <param name="CKI_NAME" value="selinux-policy: serge-testsuite"/>
      <param name="CKI_UNIVERSAL_ID" value="selinux"/>
      <param name="CKI_MAINTAINERS" value=""/>
      <param name="CKI_SELFTESTS_URL" value=""/>
      <param name="KILLTIMEOVERRIDE" value="2700"/>
    </params>
    <results>
      <result path="dio">None</result>
    </results>
  </task>
  <task id="109622843" result="Warn" status="Aborted" start_time="2020-05-15 16:52:04"
   finish_time="2020-05-15 16:58:04" duration="0:06:00">
   <fetch url="http://server/test"/>
    <params>
      <param name="CKI_NAME" value="stress: stress-ng"/>
      <param name="CKI_UNIVERSAL_ID" value="stress-ng"/>
      <param name="CKI_MAINTAINERS" value=""/>
      <param name="CKI_SELFTESTS_URL" value=""/>
      <param name="KILLTIMEOVERRIDE" value="10800"/>
      <param name="TIMEOUT" value="5"/>
    </params>
    <results>
      <result path="/10_localwatchdog">None</result>
      <result path="/99_reboot">None</result>
      <result path="/">None</result>
    </results>
  </task>
  <task id="109622844" result="Warn" status="Aborted" start_time="2020-05-15 16:52:04"
   finish_time="2020-05-15 16:58:04" duration="0:06:00">
    <params>
      <param name="CKI_NAME" value="other test"/>
      <param name="CKI_UNIVERSAL_ID" value="other-test"/>
      <param name="CKI_MAINTAINERS" value=""/>
      <param name="CKI_SELFTESTS_URL" value=""/>
      <param name="KILLTIMEOVERRIDE" value="10800"/>
      <param name="TIMEOUT" value="5"/>
    </params>
    <results>
      <result path="/">External Watchdog Expired</result>
    </results>
  </task>
</recipe>
</recipeSet>
</job>
"""


class TestProcessLog(unittest.TestCase):

    def setUp(self):
        process_logs.get_logger('test_process_logs')

    @staticmethod
    def create_file(tmpdir, file, content):
        """Create log file on {tmpdir}/recipes/{file} with {content} inside."""
        fullpath = os.path.join(tmpdir, 'recipes', file)
        pathlib.Path(os.path.dirname(fullpath)).mkdir(parents=True, exist_ok=True)
        pathlib.Path(fullpath).write_text(content)

    def test_glob_files(self):
        """Test glob_files."""
        files = [
            '8191541/logs/anaconda.log',
            '8191541/tasks/109622846/logs/taskout.log',
            '8191541/tasks/109622846/results/505987825/logs/dmesg.log',
        ]

        with tempfile.TemporaryDirectory() as tmpdir:
            for file in files:
                self.create_file(tmpdir, file, 'dummy content')

            files_list = process_logs.glob_files(f'{tmpdir}/recipes/8191541/logs/*')
            self.assertListEqual(
                [f'{tmpdir}/recipes/8191541/logs/anaconda.log'],
                files_list,
            )

            files_list = process_logs.glob_files(f'{tmpdir}/recipes/8191541/tasks/109622846/**/*')
            self.assertListEqual(
                [
                    f'{tmpdir}/recipes/8191541/tasks/109622846/logs/taskout.log',
                    f'{tmpdir}/recipes/8191541/tasks/109622846/results/505987825/logs/dmesg.log',
                ],
                files_list,
            )

    def test_recipeset(self):
        """Test Recipeset class."""
        files = [
            '8191541/logs/anaconda.log',
            '8191541/tasks/109622846/logs/harness.log',
            '8191550/logs/dmesg',
            '8191550/tasks/109622847/logs/harness.log',
            '8191550/tasks/109622848/results/505987890/logs/dmesg.log',
        ]

        with tempfile.TemporaryDirectory() as tmpdir:
            for file in files:
                self.create_file(tmpdir, file, 'dummy content')

            brs = process_logs.BeakerRecipeSet(tmpdir)

            self.assertEqual(2, len(brs.recipes))
            self.assertEqual([8191541, 8191550], list(brs.recipes.keys()))
            self.assertEqual([8191541, 8191550], [recipe.id for recipe in brs.recipes.values()])
            self.assertEqual(
                [f'{tmpdir}/recipes/8191541', f'{tmpdir}/recipes/8191550'],
                [recipe.path for recipe in brs.recipes.values()]
            )

    def test_recipe(self):
        """Test Recipe class."""
        files = [
            '8191550/logs/console.log',
            '8191550/tasks/109622847/logs/harness.log',
            '8191550/tasks/109622848/results/505987890/logs/dmesg.log',
        ]

        with tempfile.TemporaryDirectory() as tmpdir:
            for file in files:
                self.create_file(tmpdir, file, 'dummy content')

            brs = process_logs.BeakerRecipeSet(tmpdir)
            self.assertEqual(1, len(brs.recipes))
            self.assertEqual(8191550, brs.recipes[8191550].id)

            recipe = brs.recipes[8191550]
            recipe.parameters['kernel_version'] = ''
            self.assertEqual(8191550, recipe.id)
            self.assertEqual(brs, recipe.recipeset)
            self.assertEqual(f'{tmpdir}/recipes/8191550', recipe.path)
            self.assertEqual(
                [f'{tmpdir}/recipes/8191550/logs/console.log'],
                [log.path for log in recipe.logs],
            )
            self.assertEqual(2, len(recipe.tasks))
            self.assertEqual([109622847, 109622848], list(recipe.tasks.keys()))
            self.assertEqual([109622847, 109622848], [task.id for task in recipe.tasks.values()])
            self.assertEqual(
                [f'{tmpdir}/recipes/8191550/tasks/109622847', f'{tmpdir}/recipes/8191550/tasks/109622848'],
                [task.path for task in recipe.tasks.values()]
            )

    def test_task(self):
        """Test Task class."""
        files = [
            '8191550/tasks/109622846/results/505987825/logs/dmesg.log',
            '8191550/tasks/109622846/results/505987687/logs/dmesg.log',
        ]

        with tempfile.TemporaryDirectory() as tmpdir:
            for file in files:
                self.create_file(tmpdir, file, 'dummy content')

            brs = process_logs.BeakerRecipeSet(tmpdir)
            self.assertEqual(1, len(brs.recipes))
            self.assertEqual(8191550, brs.recipes[8191550].id)

            task = brs.recipes[8191550].tasks[109622846]

            self.assertEqual(109622846, task.id)
            self.assertEqual(brs.recipes[8191550], task.recipe)
            self.assertEqual(f'{tmpdir}/recipes/8191550/tasks/109622846', task.path)
            self.assertEqual(
                [
                    f'{tmpdir}/recipes/8191550/tasks/109622846/results/505987687/logs/dmesg.log',
                    f'{tmpdir}/recipes/8191550/tasks/109622846/results/505987825/logs/dmesg.log',
                ],
                [log.path for log in task.logs],
            )

    def test_merge_result_logs(self):
        """Test load_logs_from_path."""
        files = [
            ('8191541/tasks/109622846/results/dmesg.log', 'dmesg line 0\n'),
            ('8191541/tasks/109622846/results/505987825/logs/dmesg.log', 'dmesg line 2\n'),
            ('8191541/tasks/109622846/results/505987900/logs/dmesg.log', 'dmesg line 3\n'),
            ('8191541/tasks/109622846/results/505987687/logs/dmesg.log', 'dmesg line 1\n'),
            ('8191541/tasks/109622846/results/505987691/logs/resultoutputfile.log', 'result line 3\n'),
            ('8191541/tasks/109622846/results/505987690/logs/output.log', 'content'),
            ('8191541/tasks/109622846/results/505987690/logs/resultoutputfile.log', 'result line 1\nresult line 2\n'),
        ]

        with tempfile.TemporaryDirectory() as tmpdir:
            for filename, content in files:
                self.create_file(tmpdir, filename, content)

            brs = process_logs.BeakerRecipeSet(tmpdir)
            task = brs.recipes[8191541].tasks[109622846]

            task.merge_result_logs()

            self.assertListEqual(
                sorted([
                    f'{tmpdir}/recipes/8191541/tasks/109622846/results/dmesg.log',
                    f'{tmpdir}/recipes/8191541/tasks/109622846/results/505987690/logs/output.log',
                    f'{tmpdir}/recipes/8191541/tasks/109622846/results/resultoutputfile.log',
                ]),
                sorted([log.path for log in task.logs])
            )

            # Check files were merged in the correct order.
            self.assertEqual(
                'dmesg line 0\ndmesg line 1\ndmesg line 2\ndmesg line 3\n',
                pathlib.Path(f'{tmpdir}/recipes/8191541/tasks/109622846/results/dmesg.log').read_text()
            )
            self.assertEqual(
                'result line 1\nresult line 2\nresult line 3\n',
                pathlib.Path(f'{tmpdir}/recipes/8191541/tasks/109622846/results/resultoutputfile.log').read_text()
            )

    def test_aggregate_from_beaker(self):
        """
        Test aggregate_from_beaker.

        Check that the recipe and tasks has the correct CKI_ values
        on the parameters.
        """
        files = [
            '8191541/logs/anaconda.log',
            '8191541/logs/dmesg',
            '8191541/tasks/109622840/logs/taskout.log',
            '8191541/tasks/109622841/logs/taskout.log',
            '8191541/tasks/109622842/logs/taskout.log',
            '8191541/tasks/109622843/logs/taskout.log',
        ]

        tasks = {
            109622840: {'CKI_NAME': None,
                        'CKI_UNIVERSAL_ID': None},
            109622841: {'CKI_NAME': 'Boot test',
                        'CKI_UNIVERSAL_ID': 'boot'},
            109622842: {'CKI_NAME': 'selinux-policy: serge-testsuite',
                        'CKI_UNIVERSAL_ID': 'selinux',
                        'CKI_SELFTESTS_URL': '',
                        'CKI_MAINTAINERS': ''},
            109622843: {'CKI_NAME': 'stress: stress-ng',
                        'CKI_UNIVERSAL_ID': 'stress-ng',
                        'CKI_SELFTESTS_URL': '',
                        'CKI_MAINTAINERS': ''},
        }

        with tempfile.TemporaryDirectory() as tmpdir:
            pathlib.Path(os.path.join(tmpdir, '8191541.xml')).write_text(BEAKER_XML)

            for filename in files:
                self.create_file(tmpdir, filename, '')

            brs = process_logs.BeakerRecipeSet(tmpdir)
            brs.aggregate_from_beaker()

            for recipe in brs.recipes.values():
                self.assertEqual('x86_64', recipe.parameters['arch'])
                self.assertEqual('3.10.0-1137.el7.cki', recipe.parameters['kernel_version'])
                for task in recipe.tasks.values():
                    for key, value in tasks[task.id].items():
                        self.assertEqual(value, task.parameters.get(key))

    def test_dump(self):
        """
        Test dump.

        Check that the output of BeakerRecipeSet.dump is the expected.
        """
        files = [
            '8191541/logs/anaconda.log',
            '8191541/logs/dmesg',
            '8191541/tasks/109622841/logs/taskout.log',
            '8191541/tasks/109622841/results/logs/dmesg.log',
            '8191541/tasks/109622841/results/logs/output.log',
            '8191541/tasks/109622841/results/logs/resultoutputfile.log',
            '8191541/tasks/109622842/logs/taskout.log',
            '8191541/tasks/109622842/results/logs/resultoutputfile.log',
            '8191542/logs/anaconda.log',
            '8191542/tasks/109622843/results/logs/resultoutputfile.log',
            '8191542/tasks/109622844/results/logs/resultoutputfile.log',
        ]

        with tempfile.TemporaryDirectory() as tmpdir:
            pathlib.Path(os.path.join(tmpdir, '8191541.xml')).write_text(BEAKER_XML)
            pathlib.Path(os.path.join(tmpdir, '8191542.xml')).write_text(BEAKER_XML)
            for filename in files:
                self.create_file(tmpdir, filename, '')

            brs = process_logs.BeakerRecipeSet(tmpdir)
            brs.aggregate_from_beaker()

            dump_tests = brs.dump()

            self.maxDiff = None
            self.assertListEqual(
                [
                    {
                        'CKI_NAME': 'Boot test',
                        'task_id': 109622841,
                        'recipe_id': 8191541,
                        'CKI_UNIVERSAL_ID': 'boot',
                        'arch': 'x86_64',
                        'duration': '0:04:00',
                        'finish_time': '2020-05-15 16:56:04',
                        'kernel_version': '3.10.0-1137.el7.cki',
                        'result': 'Pass',
                        'start_time': '2020-05-15 16:52:04',
                        'status': 'Completed',
                        'system': 'system.beaker.redhat.com',
                        'skipped': False,
                        'fetch_url': None,
                        'output_files': [
                            {'name': 'taskout.log', 'path': 'Boot_test/8191541_x86_64_1_taskout.log',
                             'url': f'{tmpdir}/recipes/8191541/tasks/109622841/logs/taskout.log'},
                            {'name': 'dmesg.log', 'path': 'Boot_test/8191541_x86_64_1_dmesg.log',
                             'url': f'{tmpdir}/recipes/8191541/tasks/109622841/results/logs/dmesg.log'},
                            {'name': 'output.log', 'path': 'Boot_test/8191541_x86_64_1_output.log',
                             'url': f'{tmpdir}/recipes/8191541/tasks/109622841/results/logs/output.log'},
                            {'name': 'resultoutputfile.log', 'path': 'Boot_test/8191541_x86_64_1_resultoutputfile.log',
                             'url': f'{tmpdir}/recipes/8191541/tasks/109622841/results/logs/resultoutputfile.log'},
                            {'name': 'anaconda.log',
                             'path': '8191541_x86_64_1_anaconda.log',
                             'url': f'{tmpdir}/recipes/8191541/logs/anaconda.log'},
                            {'name': 'dmesg',
                             'path': '8191541_x86_64_1_dmesg',
                             'url': f'{tmpdir}/recipes/8191541/logs/dmesg'}
                        ],
                    },
                    {
                        'task_id': 109622842,
                        'recipe_id': 8191541,
                        'CKI_UNIVERSAL_ID': 'selinux',
                        'CKI_NAME': 'selinux-policy: serge-testsuite',
                        'CKI_SELFTESTS_URL': '',
                        'CKI_MAINTAINERS': '',
                        'arch': 'x86_64',
                        'duration': '0:05:00',
                        'finish_time': '2020-05-15 16:57:04',
                        'kernel_version': '3.10.0-1137.el7.cki',
                        'result': 'Pass',
                        'start_time': '2020-05-15 16:52:04',
                        'status': 'Completed',
                        'system': 'system.beaker.redhat.com',
                        'skipped': False,
                        'fetch_url': None,
                        'output_files': [
                            {'name': 'taskout.log',
                             'path': 'selinux_policy_serge_testsuite/8191541_x86_64_1_taskout.log',
                             'url': f'{tmpdir}/recipes/8191541/tasks/109622842/logs/taskout.log'},
                            {'name': 'resultoutputfile.log',
                             'path': 'selinux_policy_serge_testsuite/8191541_x86_64_1_resultoutputfile.log',
                             'url': f'{tmpdir}/recipes/8191541/tasks/109622842/results/logs/resultoutputfile.log'},
                            {'name': 'anaconda.log', 'path': '8191541_x86_64_1_anaconda.log',
                             'url': f'{tmpdir}/recipes/8191541/logs/anaconda.log'},
                            {'name': 'dmesg', 'path': '8191541_x86_64_1_dmesg',
                             'url': f'{tmpdir}/recipes/8191541/logs/dmesg'}
                        ]
                    },
                    {
                        'task_id': 109622843,
                        'recipe_id': 8191542,
                        'CKI_UNIVERSAL_ID': 'stress-ng',
                        'CKI_NAME': 'stress: stress-ng',
                        'CKI_SELFTESTS_URL': '',
                        'CKI_MAINTAINERS': '',
                        'arch': 'x86_64',
                        'duration': '0:06:00',
                        'finish_time': '2020-05-15 16:58:04',
                        'kernel_version': '3.10.0-1137.el7.cki',
                        'result': 'Warn',
                        'start_time': '2020-05-15 16:52:04',
                        'status': 'Aborted',
                        'system': 'system.beaker.redhat.com',
                        'skipped': False,
                        'fetch_url': 'http://server/test',
                        'output_files': [
                            {'name': 'resultoutputfile.log',
                             'path': 'stress_stress_ng/8191542_x86_64_2_resultoutputfile.log',
                             'url': f'{tmpdir}/recipes/8191542/tasks/109622843/results/logs/resultoutputfile.log'},
                            {'name': 'anaconda.log', 'path': '8191542_x86_64_2_anaconda.log',
                             'url': f'{tmpdir}/recipes/8191542/logs/anaconda.log'}
                        ]
                    },
                    {
                        'task_id': 109622844,
                        'recipe_id': 8191542,
                        'CKI_UNIVERSAL_ID': 'other-test',
                        'CKI_NAME': 'other test',
                        'CKI_SELFTESTS_URL': '',
                        'CKI_MAINTAINERS': '',
                        'arch': 'x86_64',
                        'duration': '0:06:00',
                        'finish_time': '2020-05-15 16:58:04',
                        'kernel_version': '3.10.0-1137.el7.cki',
                        'result': 'Warn',
                        'start_time': '2020-05-15 16:52:04',
                        'status': 'Aborted',
                        'system': 'system.beaker.redhat.com',
                        'skipped': True,
                        'fetch_url': None,
                        'output_files': [
                            {'name': 'resultoutputfile.log',
                             'path': 'other_test/8191542_x86_64_2_resultoutputfile.log',
                             'url': f'{tmpdir}/recipes/8191542/tasks/109622844/results/logs/resultoutputfile.log'},
                            {'name': 'anaconda.log', 'path': '8191542_x86_64_2_anaconda.log',
                             'url': f'{tmpdir}/recipes/8191542/logs/anaconda.log'}
                        ],
                    }
                ],
                dump_tests
            )

    def test_task_delete(self):
        """Test deleting a task."""
        files = [
            '8191550/logs/console.log',
            '8191550/tasks/109622847/logs/harness.log',
        ]

        with tempfile.TemporaryDirectory() as tmpdir:
            for file in files:
                self.create_file(tmpdir, file, 'dummy content')

            brs = process_logs.BeakerRecipeSet(tmpdir)
            recipe = brs.recipes[8191550]
            task = recipe.tasks[109622847]

            self.assertTrue(os.path.isfile(f'{tmpdir}/recipes/8191550/tasks/109622847/logs/harness.log'))

            task.delete()

            self.assertEqual(0, len(recipe.tasks))
            self.assertFalse(os.path.isfile(f'{tmpdir}/recipes/8191550/tasks/109622847/logs/harness.log'))

    def test_make_file_tree_human_friendly(self):
        """
        Test make_file_tree_human_friendly.

        Log files structure should be reorganized into a clearer
        structure before uploading.

        Test on the dump output_files output field.
        """
        files = [
            '8191550/logs/console.log',
            '8191550/tasks/109622847/logs/harness.log',
            '8191550/tasks/109622847/results/505987890/logs/taskout.log',
            '8191551/logs/console.log',
            '8191551/tasks/109622848/logs/harness.log',
        ]

        with tempfile.TemporaryDirectory() as tmpdir:
            for file in files:
                self.create_file(tmpdir, file, 'dummy content')

            brs = process_logs.BeakerRecipeSet(tmpdir)
            for recipe in brs.recipes.values():
                recipe.parameters = {
                    'arch': 'x86_64',
                    'kernel_version': '',
                }
                for task in recipe.tasks.values():
                    task.parameters = {
                        'CKI_NAME': f'test some stuff!0',
                        'CKI_UNIVERSAL_ID': 'test-somestuff'
                    }

            brs.make_file_tree_human_friendly()
            dump_tests = brs.dump()

            self.maxDiff = None
            self.assertListEqual(
                [
                    {
                        'task_id': 109622847,
                        'recipe_id': 8191550,
                        'CKI_NAME': 'test some stuff!0',
                        'CKI_UNIVERSAL_ID': 'test-somestuff',
                        'arch': 'x86_64',
                        'kernel_version': '',
                        'output_files': [
                            {'name': '8191550_x86_64_1_harness.log',
                             'path': 'test_some_stuff_0/8191550_x86_64_1_harness.log',
                             'url': f'{tmpdir}/recipes/test_some_stuff_0/8191550_x86_64_1_harness.log'},
                            {'name': '8191550_x86_64_1_taskout.log',
                             'path': 'test_some_stuff_0/8191550_x86_64_1_taskout.log',
                             'url': f'{tmpdir}/recipes/test_some_stuff_0/8191550_x86_64_1_taskout.log'},
                            {'name': '8191550_x86_64_1_console.log',
                             'path': '8191550_x86_64_1_console.log',
                             'url': f'{tmpdir}/recipes/8191550_x86_64_1_console.log'}
                        ],
                    },
                    {
                        'task_id': 109622848,
                        'recipe_id': 8191551,
                        'CKI_NAME': 'test some stuff!0',
                        'CKI_UNIVERSAL_ID': 'test-somestuff',
                        'arch': 'x86_64',
                        'kernel_version': '',
                        'output_files': [
                            {'name': '8191551_x86_64_2_harness.log',
                             'path': 'test_some_stuff_0/8191551_x86_64_2_harness.log',
                             'url': f'{tmpdir}/recipes/test_some_stuff_0/8191551_x86_64_2_harness.log'},
                            {'name': '8191551_x86_64_2_console.log',
                             'path': '8191551_x86_64_2_console.log',
                             'url': f'{tmpdir}/recipes/8191551_x86_64_2_console.log'}
                        ],
                    }
                ],
                dump_tests
            )

    def test_sanitize_string(self):
        """Test sanitize_string returns the correct value."""
        strings = [
            'some_test_name_123',
            'some test name!123',
            'some!test-name____123',
            'some_@()__test - name - 123',
        ]
        for string in strings:
            self.assertEqual(
                'some_test_name_123',
                process_logs.sanitize_string(string),
                string
            )

    def test_file_santized(self):
        console_content = """
[  128.167001] dracut: Disassembling device-mapper devices
Rebooting.
[  128.188007] reboot: Restarting system
[  128.188312] reboot: machine restart
[    0.000000] Linux version 5.6.11-4eaced5.cki (cki@runner-wbxpsiz-project-2-concurrent-0ch6tg)
[    0.000000] Command line: BOOT_IMAGE=(hd0,msdos1)/vmlinuz-5.6.11-4eaced5.cki
[    0.000000] Disabled fast string operations
[    0.000000] x86/fpu: x87 FPU will use FXSAVE
[    0.000000] BIOS-provided physical RAM map:
"""
        files = [
            ('8191541/logs/console.log', console_content),
            ('8191541/tasks/109622847/logs/harness.log', ''),
        ]

        with tempfile.TemporaryDirectory() as tmpdir:
            for file, content in files:
                self.create_file(tmpdir, file, content)

            pathlib.Path(os.path.join(tmpdir, '8191541.xml')).write_text(BEAKER_XML)

            brs = process_logs.BeakerRecipeSet(tmpdir)
            brs.aggregate_from_beaker()

    @staticmethod
    def create_task_from_results(results):
        """Return task xml for results."""
        generic_task = """
        <task name="Task name" role="STANDALONE" id="110386664" result="Warn" status="Aborted">
          <params><param name="CKI_NAME" value="Task Name"/></params>
          <results>{results}</results>
        </task>
        """
        return ET.fromstring(
            generic_task.format(results=results)
        )

    def test_task_skipped(self):
        """Test the 'skipped' detect on Task.aggregate_from_beaker with different cases."""
        class MockRecipe:
            path = 'mockrecipe'

        test_cases = (
            # (task results xml, skipped expected value),
            # Only one EWD Expired result.
            ('<result path="/" result="Warn" id="510111173">External Watchdog Expired</result>', True),
            # Only one result in path /
            ('<result path="/" result="Warn" id="510111173"></result>', True),
            # One test that run before the result with path "/"
            ('<result path="/test" result="Warn" id="510111173"></result>' +
             '<result path="/" result="Warn" id="510111173"></result>', False),
            # Real case with Oops and EWD.
            ('<result path="/test" result="Pass" id="510111173"><logs></logs></result>' +
             '<result path="/" result="Panic" id="510111173">Oops.</result>' +
             '<result path="/" result="Warn" id="510111173">External Watchdog Expired</result>', False),
            # Some combination of things.
            ('<result path="/test" result="Pass" id="510111173"></result>' +
             '<result path="/" result="Warn" id="510111173">External Watchdog Expired</result>' +
             '<result path="/" result="Warn" id="510111173"></result>', False),
            # Panic 😱.
            ('<result path="/" result="Panic" id="510111173">Oops.</result>' +
             '<result path="/" result="Warn" id="510111173">External Watchdog Expired</result>' +
             '<result path="/" result="Warn" id="510111173"></result>', False),
            ('<result path="/" result="Panic" id="509156970">kernel BUG at lib/list_debug.c:51!</result>' +
             '<result path="/" result="Warn" id="509158774">External Watchdog Expired</result>', False),
        )

        for results, expected in test_cases:
            task = process_logs.BeakerTask(MockRecipe(), 110386664)
            task.aggregate_from_beaker_xml(self.create_task_from_results(results))

            self.assertEqual(expected, task.parameters['skipped'], results)
