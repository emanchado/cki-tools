import unittest

import botocore
from cki.cki_tools import _utils


class TestS3Client(unittest.TestCase):
    def test_init(self):
        """Test init sets all attributes."""
        spec = _utils.BucketSpec('https://endpoint', 'access_key', 'secret_key', 'bucket', 'prefix')
        bucket = _utils.S3Bucket(spec)

        self.assertEqual(spec, bucket.spec)

    def test_client(self):
        """Test client."""
        spec = _utils.BucketSpec('https://endpoint', 'access_key', 'secret_key', 'bucket', 'prefix')
        bucket = _utils.S3Bucket(spec)

        self.assertIsInstance(bucket.client, botocore.client.BaseClient)

    def test_init_from_string(self):
        """Test from_bucket_string classmethod."""
        spec = _utils.BucketSpec('https://endpoint', 'access_key', 'secret_key', 'bucket', 'prefix')
        bucket = _utils.S3Bucket.from_bucket_string('https://endpoint|access_key|secret_key|bucket|prefix')

        self.assertEqual(spec.endpoint, bucket.spec.endpoint)


class TestParseBucketSpec(unittest.TestCase):
    def test_endpoint(self):
        """Test endpoint parameter."""
        test_cases = (
            ('https://endpoint||||', 'https://endpoint'),
            ('https://endpoint/||||', 'https://endpoint'),
            ('||||', 'http://s3.amazonaws.com'),
        )
        for case, expected in test_cases:
            self.assertEqual(expected, _utils.parse_bucket_spec(case).endpoint, (case, expected))

    def test_prefix(self):
        """Test prefix parameter."""
        test_cases = (
            ('||||path', 'path/'),
            ('||||path/', 'path/'),
            ('||||', ''),
        )
        for case, expected in test_cases:
            self.assertEqual(expected, _utils.parse_bucket_spec(case).prefix, (case, expected))

    def test_attributes(self):
        """Test all BucketSpec attributes."""
        spec = _utils.BucketSpec('https://endpoint', 'access_key', 'secret_key', 'bucket', 'prefix/')
        bucket = _utils.parse_bucket_spec('https://endpoint|access_key|secret_key|bucket|prefix')

        for attribute in ('endpoint', 'access_key', 'secret_key', 'bucket', 'prefix'):
            self.assertEqual(
                getattr(spec, attribute),
                getattr(bucket, attribute),
            )
