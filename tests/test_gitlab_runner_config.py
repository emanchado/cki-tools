"""Tests for gitlab runner config."""
import json
import os
import pathlib
import tempfile
import unittest

import responses
import yaml

from cki.deployment_tools.gitlab_runner_config import main


class TestRunnerConfig(unittest.TestCase):
    """Base class with a common test bed."""

    maxDiff = 10000
    config = {
        'runner_tokens': {
            'c1-d1-kubernetes-low': 'runner-token-c1-d1-kubernetes-low',
            'c1-d2-docker-high': 'runner-token-c1-d2-docker-high',
            'c1-d2-docker-low': 'runner-token-c1-d2-docker-low',
            'c1-d2-docker-priv': 'runner-token-c1-d2-docker-priv',
            'c2-d2-docker-low': 'runner-token-c2-d2-docker-low',
            'c2-d2-docker-priv': 'runner-token-c2-d2-docker-priv',
        },
        'gitlab_instances': {
            'i1': {'url': 'https://url1', 'api_token': 'token1',
                   'registration_tokens': {'g1': 'g1-token',
                                           'g1/p1': 'p1-token'}},
            'i2': {'url': 'https://url2', 'api_token': 'token2'},
        },
        'runner_deployments': {
            '.default': {'.metadata': {'name': 'gitlab-runner', },
                         '.filename': 'config.toml'},
            'd1': {'.executor': 'kubernetes', '.configmap': 'd1.yaml'},
            'd2': {'.executor': 'docker', '.configfile': 'd2.toml'}
        },
        'runner_templates': {
            'docker-high': {'executor': 'docker',
                            '.tags': {'default': [], 'fb': 'f1'}},
            'docker-low': {'executor': 'docker',
                           '.tags': {'default': 'r1', 'fb': ['f2', 'f3']}},
            'docker-priv': {'executor': 'docker',
                            '.tags': {'default': 'r2'}},
            'kubernetes-low': {'executor': 'kubernetes',
                               'key': 'value',
                               '.tags': {'default': 'r3', 'fb': []}}
        },
        'variable_groups': {
            'var1': {'key': 'value', 'key2': '$env_value2'},
            'var2': {'.extends': 'var1', 'key2': 'value3'},
            'var3': {'key3': 'value3'},
        },
        'runner_configurations': {
            'c1': {
                'runner_deployments': {
                    'd1': ['kubernetes-low'],
                    'd2': ['docker-high', 'docker-low', 'docker-priv']},
                'variable_sets': ['var1'],
            },
            'c2': {
                'runner_deployments': {
                    'd1': [],
                    'd2': ['docker-low', 'docker-priv']
                },
            }
        },
        'runner_registrations': [
            {'runner_configurations': 'c1', 'gitlab_groups': 'i1/g1'},
            {'runner_configurations': 'c2', 'gitlab_projects': ['i1/g1/p1',
                                                                'i1/g1/p2']},
        ],
        'gitlab_variables': [
            {'gitlab_groups': 'i1/g1', 'variable_group': '$env_var1'},
            {'gitlab_projects': 'i1/g1/p1', 'variable_group': 'var2'},
            {'gitlab_projects': 'i2/g2/p2', 'variable_group': 'var3'},
        ],
    }

    def setUp(self):
        responses.add(responses.GET,
                      url='https://url1/api/v4/groups/g1/variables',
                      json=[{'key': 'key', 'value': 'value'}])
        responses.add(responses.GET,
                      url='https://url1/api/v4/projects/g1%2Fp1/variables',
                      json=[{'key': 'key', 'value': 'value', },
                            {'key': 'key2', 'value': 'value2'}])
        responses.add(responses.GET,
                      url='https://url2/api/v4/projects/g2%2Fp2/variables',
                      json=[{'key': 'key3', 'value': 'value3'},
                            {'key': 'key4', 'value': 'value4'}])
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners',
                      json=[
                          {'id': 1, 'description': 'c1-d2-docker-high'},
                          {'id': 2, 'description': 'c1-d2-docker-low'},
                          {'id': 3, 'description': 'c1-d2-docker-priv'},
                          {'id': 4, 'description': 'c2-d2-docker-low'}])
        responses.add(responses.GET,
                      url='https://url2/api/v4/runners',
                      json=[])
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners/1',
                      json={'id': 1, 'description': 'c1-d2-docker-high',
                            'projects': [], 'tag_list': ['t1'],
                            'groups': [{'id': 1,
                                        'web_url': 'https://url1/groups/g1'}],
                            'access_level': 'not_protected',
                            'active': True,
                            'locked': True,
                            'run_untagged': False,
                            'maximum_timeout': 24 * 60 * 60})
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners/2',
                      json={'id': 2, 'description': 'c1-d2-docker-low',
                            'projects': [], 'tag_list': ['r2'],
                            'groups': [{'id': 1,
                                        'web_url': 'https://url1/groups/g1'}],
                            'access_level': 'not_protected',
                            'active': True,
                            'locked': True,
                            'run_untagged': False,
                            'maximum_timeout': 24 * 60 * 60})
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners/3',
                      json={'id': 3, 'description': 'c1-d2-docker-priv',
                            'projects': [], 'tag_list': [],
                            'groups': [{'id': 1,
                                        'web_url': 'https://url1/groups/g1'}],
                            'access_level': 'not_protected',
                            'active': True,
                            'locked': True,
                            'run_untagged': False,
                            'maximum_timeout': 24 * 60 * 60})
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners/4',
                      json={'id': 4, 'description': 'c2-d2-docker-low',
                            'projects': [{'id': 1,
                                          'web_url': 'https://url1/g1/p1'},
                                         {'id': 3,
                                          'web_url': 'https://url1/g1/p3'}],
                            'groups': [], 'tag_list': [],
                            'access_level': 'not_protected',
                            'active': True,
                            'locked': True,
                            'run_untagged': False,
                            'maximum_timeout': 24 * 60 * 60})
        responses.add(responses.GET,
                      url='https://url1/api/v4/groups/1',
                      json={'full_path': 'g1'})
        responses.add(responses.GET,
                      url='https://url1/api/v4/groups/2',
                      json={'full_path': 'g2'})
        responses.add(responses.GET,
                      url='https://url1/api/v4/projects/1',
                      json={'path_with_namespace': 'g1/p1'})
        responses.add(responses.GET,
                      url='https://url1/api/v4/projects/3',
                      json={'path_with_namespace': 'g1/p3'})

    def _main(self, args):
        with tempfile.NamedTemporaryFile('w+t') as config_file:
            config_file.write(yaml.safe_dump(self.config))
            config_file.flush()
            result = main(['--config', config_file.name] + args)
            if args[1] == 'diff':
                return result.strip().split('\n')
            if args[1] == 'apply':
                return None
            return yaml.safe_load(result)

    @staticmethod
    def _requests(method=None, url=None, parse=True):
        return [json.loads(c.request.body) if parse else c.request.body
                for c in responses.calls
                if (not method or c.request.method == method) and
                (not url or c.request.url == url)]


@unittest.mock.patch.dict(os.environ, {'env_value2': 'value2',
                                       'env_var1': 'var1'})
class TestVariables(TestRunnerConfig):
    """Tests the variables functionality."""

    @responses.activate
    def test_dump(self):
        """Check the dumping of project variables."""
        result = self._main(['variables', 'dump'])
        self.assertEqual(result, {
            'groups': {
                f'i1/g1': {'key': 'value'}},
            'projects': {
                'i1/g1/p1': {'key': 'value', 'key2': 'value2'},
                'i2/g2/p2': {'key3': 'value3', 'key4': 'value4'}}})

    def test_generate(self):
        """Check the generation of project variables."""
        result = self._main(['variables', 'generate'])
        self.assertEqual(result, {
            'groups': {
                'i1/g1': {'key': 'value', 'key2': 'value2'}},
            'projects': {
                'i1/g1/p1': {'key': 'value', 'key2': 'value3'},
                'i2/g2/p2': {'key3': 'value3', }}})

    @responses.activate
    def test_diff(self):
        """Check the diffing of project variables."""
        changes = self._main(['variables', 'diff'])
        self.assertEqual(changes, [
            '--- current',
            '',
            '+++ proposed',
            '',
            '@@ -1,11 +1,11 @@',
            '',
            ' groups:',
            '   i1/g1:',
            '     key: value',
            '+    key2: value2',
            ' projects:',
            '   i1/g1/p1:',
            '     key: value',
            '-    key2: value2',
            '+    key2: value3',
            '   i2/g2/p2:',
            '     key3: value3',
            '-    key4: value4',
        ])

    @responses.activate
    def test_apply(self):
        """Check the application of project variables."""
        post_url = 'https://url1/api/v4/groups/g1/variables'
        put_url = 'https://url1/api/v4/projects/g1%2Fp1/variables/key2'
        delete_url = f'https://url2/api/v4/projects/g2%2Fp2/variables/key4'
        responses.add(responses.POST, post_url, json={})
        responses.add(responses.PUT, put_url, json={})
        responses.add(responses.DELETE, delete_url, json={})
        self._main(['variables', 'apply'])
        self.assertEqual(self._requests(method='POST'), [
            {'key': 'key2', 'value': 'value2', 'variable_type': 'env_var'}])
        self.assertEqual(self._requests(method='PUT'), [
            {'key': 'key2', 'value': 'value3', 'variable_type': 'env_var'}])
        self.assertEqual(self._requests(method='DELETE', parse=False), [None])


@unittest.mock.patch.dict(os.environ, {'env_value2': 'value2',
                                       'env_var1': 'var1'})
class TestRegistrations(TestRunnerConfig):
    """Tests the registrations functionality."""

    @responses.activate
    def test_dump(self):
        """Check the dumping of registrations."""
        result = self._main(['registrations', 'dump'])
        self.assertEqual(result, {
            'c1-d2-docker-high': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': ['t1'],
                'access_level': 'not_protected',
                'active': True,
                'description': 'c1-d2-docker-high',
                'locked': True,
                'maximum_timeout': 86400,
                'run_untagged': False,
            },
            'c1-d2-docker-low': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': ['r2'],
                'access_level': 'not_protected',
                'active': True,
                'description': 'c1-d2-docker-low',
                'locked': True,
                'maximum_timeout': 86400,
                'run_untagged': False,
            },
            'c1-d2-docker-priv': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': [],
                'access_level': 'not_protected',
                'active': True,
                'description': 'c1-d2-docker-priv',
                'locked': True,
                'maximum_timeout': 86400,
                'run_untagged': False,
            },
            'c2-d2-docker-low': {
                '.groups': [],
                '.projects': ['i1/g1/p1', 'i1/g1/p3'],
                'tag_list': [],
                'access_level': 'not_protected',
                'active': True,
                'description': 'c2-d2-docker-low',
                'locked': True,
                'maximum_timeout': 86400,
                'run_untagged': False,

            }
        })

    def test_generate(self):
        """Check the generation of registrations."""
        result = self._main(['registrations', 'generate'])
        self.assertEqual(result, {
            'c1-d1-kubernetes-low': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': ['r3'],
                'access_level': 'not_protected',
                'active': True,
                'description': 'c1-d1-kubernetes-low',
                'locked': True,
                'maximum_timeout': 604800,
                'run_untagged': False,
            },
            'c1-d2-docker-high': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': [],
                'access_level': 'not_protected',
                'active': True,
                'description': 'c1-d2-docker-high',
                'locked': True,
                'maximum_timeout': 604800,
                'run_untagged': False,
            },
            'c1-d2-docker-low': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': ['r1'],
                'access_level': 'not_protected',
                'active': True,
                'description': 'c1-d2-docker-low',
                'locked': True,
                'maximum_timeout': 604800,
                'run_untagged': False,
            },
            'c1-d2-docker-priv': {
                '.groups': ['i1/g1'],
                '.projects': [],
                'tag_list': ['r2'],
                'access_level': 'not_protected',
                'active': True,
                'description': 'c1-d2-docker-priv',
                'locked': True,
                'maximum_timeout': 604800,
                'run_untagged': False,
            },
            'c2-d2-docker-low': {
                '.groups': [],
                '.projects': ['i1/g1/p1', 'i1/g1/p2'],
                'tag_list': ['r1'],
                'access_level': 'not_protected',
                'active': True,
                'description': 'c2-d2-docker-low',
                'locked': True,
                'maximum_timeout': 604800,
                'run_untagged': False,
            },
            'c2-d2-docker-priv': {
                '.groups': [],
                '.projects': ['i1/g1/p1', 'i1/g1/p2'],
                'tag_list': ['r2'],
                'access_level': 'not_protected',
                'active': True,
                'description': 'c2-d2-docker-priv',
                'locked': True,
                'maximum_timeout': 604800,
                'run_untagged': False,
            },
        })

    def test_tags(self):
        """Check the generation of registrations."""
        result = self._main(['registrations', 'generate',
                             '--tag-assignment', 'fb'])
        self.assertEqual(result['c1-d1-kubernetes-low']['tag_list'], [])
        self.assertEqual(result['c1-d2-docker-high']['tag_list'], ['f1'])
        self.assertEqual(result['c1-d2-docker-low']['tag_list'], ['f2', 'f3'])
        self.assertEqual(result['c1-d2-docker-priv']['tag_list'], [])
        self.assertEqual(result['c2-d2-docker-low']['tag_list'], ['f2', 'f3'])
        self.assertEqual(result['c2-d2-docker-priv']['tag_list'], [])

    @responses.activate
    def test_diff(self):
        """Check the diffing of registrations."""
        changes = self._main(['registrations', 'diff'])
        self.assertEqual(changes, [
            '--- current',
            '',
            '+++ proposed',
            '',
            '@@ -1,48 +1,74 @@',
            '',
            '+c1-d1-kubernetes-low:',
            '+  .groups:',
            '+  - i1/g1',
            '+  .projects: []',
            '+  access_level: not_protected',
            '+  active: true',
            '+  description: c1-d1-kubernetes-low',
            '+  locked: true',
            '+  maximum_timeout: 604800',
            '+  run_untagged: false',
            '+  tag_list:',
            '+  - r3',
            ' c1-d2-docker-high:',
            '   .groups:',
            '   - i1/g1',
            '   .projects: []',
            '   access_level: not_protected',
            '   active: true',
            '   description: c1-d2-docker-high',
            '   locked: true',
            '-  maximum_timeout: 86400',
            '+  maximum_timeout: 604800',
            '   run_untagged: false',
            '-  tag_list:',
            '-  - t1',
            '+  tag_list: []',
            ' c1-d2-docker-low:',
            '   .groups:',
            '   - i1/g1',
            '   .projects: []',
            '   access_level: not_protected',
            '   active: true',
            '   description: c1-d2-docker-low',
            '   locked: true',
            '-  maximum_timeout: 86400',
            '+  maximum_timeout: 604800',
            '   run_untagged: false',
            '   tag_list:',
            '-  - r2',
            '+  - r1',
            ' c1-d2-docker-priv:',
            '   .groups:',
            '   - i1/g1',
            '   .projects: []',
            '   access_level: not_protected',
            '   active: true',
            '   description: c1-d2-docker-priv',
            '   locked: true',
            '-  maximum_timeout: 86400',
            '+  maximum_timeout: 604800',
            '   run_untagged: false',
            '-  tag_list: []',
            '+  tag_list:',
            '+  - r2',
            ' c2-d2-docker-low:',
            '   .groups: []',
            '   .projects:',
            '   - i1/g1/p1',
            '-  - i1/g1/p3',
            '+  - i1/g1/p2',
            '   access_level: not_protected',
            '   active: true',
            '   description: c2-d2-docker-low',
            '   locked: true',
            '-  maximum_timeout: 86400',
            '+  maximum_timeout: 604800',
            '   run_untagged: false',
            '-  tag_list: []',
            '+  tag_list:',
            '+  - r1',
            '+c2-d2-docker-priv:',
            '+  .groups: []',
            '+  .projects:',
            '+  - i1/g1/p1',
            '+  - i1/g1/p2',
            '+  access_level: not_protected',
            '+  active: true',
            '+  description: c2-d2-docker-priv',
            '+  locked: true',
            '+  maximum_timeout: 604800',
            '+  run_untagged: false',
            '+  tag_list:',
            '+  - r2'
        ])

    @responses.activate
    def test_no_create_missing(self):
        """Check that --create-missing is needed."""
        self.assertRaises(Exception,
                          lambda: self._main(['registrations', 'apply']))

    @responses.activate
    def test_apply(self):
        """Check the application of registrations."""
        post_url1 = 'https://url1/api/v4/runners'
        post_url2 = 'https://url1/api/v4/projects/g1%2Fp2/runners'
        put_url1 = 'https://url1/api/v4/runners/1'
        put_url2 = 'https://url1/api/v4/runners/2'
        put_url3 = 'https://url1/api/v4/runners/3'
        put_url4 = 'https://url1/api/v4/runners/4'
        put_url5 = 'https://url1/api/v4/runners/6'
        delete_url1 = f'https://url1/api/v4/projects/g1%2Fp3/runners/4'
        responses.add(responses.POST, post_url1,
                      json={'id': 5, 'token': 'new-token-5'})
        responses.add(responses.POST, post_url1,
                      json={'id': 6, 'token': 'new-token-6'})
        responses.add(responses.POST, post_url2, json={})
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners/5',
                      json={'id': 5, 'description': 'c1-d1-kubernetes-low',
                            'groups': [{'id': 1}],
                            'projects': [],
                            'tag_list': ['r3'],
                            'access_level': 'not_protected',
                            'active': True,
                            'locked': True,
                            'run_untagged': False,
                            'maximum_timeout': 7 * 24 * 60 * 60})
        responses.add(responses.GET,
                      url='https://url1/api/v4/runners/6',
                      json={'id': 6, 'description': 'c2-d2-docker-priv',
                            'groups': [],
                            'projects': [{'id': 1}],
                            'tag_list': ['r2'],
                            'access_level': 'not_protected',
                            'active': True,
                            'locked': True,
                            'run_untagged': False,
                            'maximum_timeout': 7 * 24 * 60 * 60})
        responses.add(responses.PUT, put_url1, json={})
        responses.add(responses.PUT, put_url2, json={})
        responses.add(responses.PUT, put_url3, json={})
        responses.add(responses.PUT, put_url4, json={})
        responses.add(responses.PUT, put_url5, json={})
        responses.add(responses.DELETE, delete_url1, json={})

        self._main(['registrations', 'apply', '--create-missing'])

        self.assertEqual(self._requests(method='POST', url=post_url1), [
            {'token': 'g1-token',
             'description': 'c1-d1-kubernetes-low',
             'tag_list': ['r3'],
             'access_level': 'not_protected',
             'active': True,
             'locked': True,
             'run_untagged': False,
             'maximum_timeout': 7 * 24 * 60 * 60},
            {'token': 'p1-token',
             'description': 'c2-d2-docker-priv',
             'tag_list': ['r2'],
             'access_level': 'not_protected',
             'active': True,
             'locked': True,
             'run_untagged': False,
             'maximum_timeout': 7 * 24 * 60 * 60}])
        self.assertEqual(self._requests(method='POST', url=post_url2), [
            {'runner_id': 4}, {'runner_id': 6}])
        self.assertEqual(self._requests(method='PUT', url=put_url1), [
            {'access_level': 'not_protected',
             'active': True,
             'description': 'c1-d2-docker-high',
             'locked': True,
             'maximum_timeout': 604800,
             'run_untagged': False,
             'tag_list': []}])
        self.assertEqual(self._requests(method='PUT', url=put_url2), [
            {'access_level': 'not_protected',
             'active': True,
             'description': 'c1-d2-docker-low',
             'locked': True,
             'maximum_timeout': 604800,
             'run_untagged': False,
             'tag_list': ['r1']}])
        self.assertEqual(self._requests(method='PUT', url=put_url3), [
            {'access_level': 'not_protected',
             'active': True,
             'description': 'c1-d2-docker-priv',
             'locked': True,
             'maximum_timeout': 604800,
             'run_untagged': False,
             'tag_list': ['r2']}])
        self.assertEqual(self._requests(method='PUT', url=put_url4), [
            {'access_level': 'not_protected',
             'active': True,
             'description': 'c2-d2-docker-low',
             'locked': True,
             'maximum_timeout': 604800,
             'run_untagged': False,
             'tag_list': ['r1']},
            {'locked': False}, {'locked': True}])
        self.assertEqual(self._requests(method='PUT', url=put_url5), [
            {'locked': False}, {'locked': True}])
        self.assertEqual(self._requests(url=delete_url1, parse=False), [None])


@unittest.mock.patch.dict(os.environ, {'env_value2': 'value2',
                                       'env_var1': 'var1'})
class TestConfigurations(TestRunnerConfig):
    """Tests the configurations functionality."""

    d1 = ('apiVersion: v1\n' +
          'kind: ConfigMap\n' +
          'metadata:\n' +
          '  name: gitlab-runner\n' +
          'data:\n' +
          '  config.toml: |+\n' +
          '    [[runners]]\n' +
          '    executor = "kubernetes"\n' +
          '    key = "value"\n' +
          '    name = "c1-d1-kubernetes-low"\n' +
          '    token = "runner-token-c1-d1-kubernetes-low"\n' +
          '    url = "https://url1"\n' +
          '\n' +
          '...\n')
    d2 = ('[[runners]]\n' +
          'executor = "docker"\n' +
          'name = "c1-d2-docker-high"\n' +
          'token = "runner-token-c1-d2-docker-high"\n' +
          'url = "https://url1"\n' +
          '\n' +
          '[[runners]]\n' +
          'executor = "docker"\n' +
          'name = "c1-d2-docker-low"\n' +
          'token = "runner-token-c1-d2-docker-low"\n' +
          'url = "https://url1"\n' +
          '\n' +
          '[[runners]]\n' +
          'executor = "docker"\n' +
          'name = "c1-d2-docker-priv"\n' +
          'token = "runner-token-c1-d2-docker-priv"\n' +
          'url = "https://url1"\n' +
          '\n' +
          '[[runners]]\n' +
          'executor = "docker"\n' +
          'name = "c2-d2-docker-low"\n' +
          'token = "runner-token-c2-d2-docker-low"\n' +
          'url = "https://url1"\n' +
          '\n' +
          '[[runners]]\n' +
          'executor = "docker"\n' +
          'name = "c2-d2-docker-priv"\n' +
          'token = "runner-token-c2-d2-docker-priv"\n' +
          'url = "https://url1"\n' +
          '\n')

    def test_dump(self):
        """Check the dumping of configurations."""
        with tempfile.TemporaryDirectory() as directory:
            pathlib.Path(directory, 'd1.yaml').write_text(self.d1)
            pathlib.Path(directory, 'd2.toml').write_text(self.d2)
            result = self._main(['configurations', 'dump',
                                 '--directory', directory])
        self.assertEqual(result, {
            'd1.yaml': self.d1,
            'd2.toml': self.d2
        })

    def test_generate(self):
        """Check the generation of configurations."""
        result = self._main(['configurations', 'generate'])
        self.assertEqual(result, {
            'd1.yaml': self.d1,
            'd2.toml': self.d2
        })

    def test_diff(self):
        """Check the diffing of configurations."""
        with tempfile.TemporaryDirectory() as directory:
            pathlib.Path(directory, 'd1.yaml').write_text(
                self.d1.replace('"value"', '"value2"'))
            pathlib.Path(directory, 'd2.toml').write_text(self.d2)
            changes = self._main(['configurations', 'diff',
                                  '--directory', directory,
                                  '--context', '1'])
        self.assertEqual(changes, [
            '--- current',
            '',
            '+++ proposed',
            '',
            '@@ -9,3 +9,3 @@',
            '',
            '       executor = "kubernetes"',
            '-      key = "value2"',
            '+      key = "value"',
            '       name = "c1-d1-kubernetes-low"',
        ])

    def test_apply(self):
        """Check the application of configurations."""
        with tempfile.TemporaryDirectory() as directory:
            pathlib.Path(directory, 'd1.yaml').write_text(
                self.d1.replace('"value"', '"value2"'))
            pathlib.Path(directory, 'd2.toml').write_text(self.d2)
            self._main(['configurations', 'apply',
                        '--directory', directory])
            self.assertEqual(pathlib.Path(directory, 'd1.yaml').read_text(),
                             self.d1)
            self.assertEqual(pathlib.Path(directory, 'd2.toml').read_text(),
                             self.d2)
