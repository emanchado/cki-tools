"""Gitlab api interaction tests."""
import json
import os
import unittest
from unittest import mock

from cki.cki_tools import webhook_receiver

SECRET = 'secret'


@mock.patch.dict(os.environ, {'WEBHOOK_RECEIVER_WEBSECRET': SECRET,
                              'IS_PRODUCTION': 'True'})
class TestWebhook(unittest.TestCase):
    """ Test webhook receiver."""

    def setUp(self):
        """SetUp class."""
        self.client = webhook_receiver.app.test_client()

    def _check_send(self, data):
        response = self.client.post(
            '/', json=data, headers={'X-Gitlab-Token': SECRET})
        self.assertEqual(response.status, '200 OK')

    @staticmethod
    def _publish_call(data, routing_key):
        return mock.call(
            exchange=webhook_receiver.RABBITMQ_EXCHANGE,
            routing_key=routing_key,
            body=json.dumps(data))

    def _check_event(self, data, routing_key, connection):
        self._check_send(data)
        self.assertEqual(connection().channel().basic_publish.mock_calls,
                         [self._publish_call(data, routing_key)])

    @ mock.patch('cki_lib.messagequeue.pika.BlockingConnection')
    def test_job_event(self, connection):
        """Check routing of a job event."""
        self._check_event({
            'object_kind': 'build',
            'repository': {
                'homepage': 'https://host.name:1234/group/subgroup/project',
            }
        }, 'host.name.group.subgroup.project.build', connection)

    @ mock.patch('cki_lib.messagequeue.pika.BlockingConnection')
    def test_pipeline_event(self, connection):
        """Check routing of a pipeline event."""
        self._check_event({
            'object_kind': 'pipeline',
            'project': {
                'web_url': 'https://host.name:1234/group/subgroup/project',
            },
        }, 'host.name.group.subgroup.project.pipeline', connection)

    def test_entrypoint_missing(self):
        """Check a missing entrypoint."""
        response = self.client.post('/non-existent', json={})
        self.assertEqual(response.status, '404 NOT FOUND')

    def test_websecret_missing(self):
        """Check a missing websecret."""
        response = self.client.post('/', json={})
        self.assertEqual(response.status, '403 FORBIDDEN')

    def test_websecret_wrong(self):
        """Check a wrong websecret."""
        response = self.client.post(
            '/', json={}, headers={'X-Gitlab-Token': 'bad_secret'})
        self.assertEqual(response.status, '403 FORBIDDEN')

    @ mock.patch.dict(os.environ, {'WEBHOOK_RECEIVER_WEBSECRET': ''})
    def test_websecret_env_missing(self):
        """Check a missing websecret env variable."""
        response = self.client.post('/', json={})
        self.assertEqual(response.status, '404 NOT FOUND')

    def test_missing_object_kind(self):
        """Check a missing object_kind."""
        response = self.client.post(
            '/', json={}, headers={'X-Gitlab-Token': SECRET})
        self.assertEqual(response.status, '500 INTERNAL SERVER ERROR')

    @ mock.patch('cki_lib.messagequeue.pika.BlockingConnection')
    def test_errors(self, connection):
        """Check transient error handling."""
        routing_key = 'host.group.subgroup.project.build'
        messages = [{
            'index': index,
            'object_kind': 'build',
            'repository': {
                'homepage': 'https://host:1234/group/subgroup/project',
            },
        } for index in range(3)]

        connection().channel().basic_publish.side_effect = [
            OSError('first'), OSError('second'), True, True, True]

        self._check_send(messages[0])
        self._check_send(messages[1])
        self.assertEqual(connection().channel().basic_publish.mock_calls,
                         [self._publish_call(messages[0], routing_key)] * 2)

        connection().channel().basic_publish.mock_calls = []
        self._check_send(messages[2])
        self.assertEqual(connection().channel().basic_publish.mock_calls,
                         [self._publish_call(messages[i], routing_key)
                          for i in range(3)])
