# CKI Tools

All the command line tools the CKI team uses.

## Optional dependencies

To use the `brew_trigger` module, use the `brew` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-tools.git/#egg=cki_tools[brew]
```

To use the `kcidb` module, use the `kcidb` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-tools.git/#egg=cki_tools[kcidb]
```

To use the `beaker-respin` CLI, use the `beaker_respin` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-tools.git/#egg=cki_tools[beaker_respin]
```

## `beaker_tools.respin`

```shell
python3 -m cki.beaker_tools.respin [options]
```

See the command line help for details.

## `brew_trigger`

```shell
python3 -m pipeline_tools.brew_trigger [-c CONFIG] [options] task_id
```

Trigger a CKI pipeline for a kernel from Brew/Koji. The same configuration
files as for the [brew-trigger pipeline-trigger module] can be used. If
`IS_PRODUCTION` variable is not present or set to `False`, retriggered pipelines
are created instead of production.

[brew-trigger pipeline-trigger module]: https://gitlab.com/cki-project/pipeline-trigger/-/blob/master/triggers/brew_trigger.py

## `cki.deployment_tools.gitlab_runner_config`

This module is used to ensure a managed and reproducible gitlab-runner setup.

### CLI interface

```shell
python3 -m cki.deployment_tools.gitlab_runner_config \
    [-c CONFIG] [options] OBJECT ACTION
```

A gitlab-runner setup can be quite nicely split into the following distinct
objects that can be managed separately:

- `configurations`: gitlab-runner configurations, i.e. the `config.toml` files
  used to configure the gitlab-runner processes, optionally embedded in a
  Kubernetes ConfigMap
- `registrations`: gitlab-runner registrations with GitLab projects or groups
- `variables`: CI/CD variables for GitLab projects or groups

The following actions can be performed on them:

- `dump`: get the currently deployed setup, and output it in YAML format
- `generate`: generate the new setup from the configuration, and output it in
  YAML format
- `diff`: show the difference between the currently deployed and the newly
  generated setup
- `apply`: adjust the deployed setup to match the configuration

To register new runners with a GitLab instance, the `--create-missing`
parameter must be specified. The printed runner authorization token must be
added to the `runner_tokens` field of the configuration file. It is considered
a secret and cannot be obtained from the GitLab instance later on!

### Configuration file

The desired gitlab-runner setup is described by a YAML configuration file.
In the following sections, the configuration sections are described based on a
[non-trivial example](gitlab-runner-example.yaml).

The example describes a gitlab-runner setup across two GitLab instances,
`gitlab.com` and `gitlab.corp.com`. On the first (public) instance, all
projects in the `external-group` group should have access to a set of gitlab
runners. On the second (internal) instance, all projects in the
`internal-group` group should have access to a set of identically configured
gitlab runners. Three runners should be configured: two in an OpenShift 3
project (high and normal), and one privileged docker runner for building
container images. There is one project `project1` in the internal group that
needs a slightly different setup. For this project, the OpenShift runners
should be deployed into a different OpenShift 4 project. Additionally, this
project should have access to a password via a CI/CD variable.

#### `runner_tokens`

This dictionary contains the runner authentication tokens indexed by the
generated full name.

Example:

```yaml
runner_tokens:
  com-ocp3-kubernetes-high: $COM_OCP3_KUBERNETES_HIGH_RUNNER_TOKEN
  com-ocp3-kubernetes-normal: $COM_OCP3_KUBERNETES_NORMAL_RUNNER_TOKEN
  com-server-docker-priv: $COM_SERVER_DOCKER_PRIV_RUNNER_TOKEN
  corp-ocp3-kubernetes-high: $CORP_OCP3_KUBERNETES_HIGH_RUNNER_TOKEN
  corp-ocp3-kubernetes-normal: $CORP_OCP3_KUBERNETES_NORMAL_RUNNER_TOKEN
  corp-server-docker-priv: $CORP_SERVER_DOCKER_PRIV_RUNNER_TOKEN
  project1-ocp4-kubernetes-normal: $PROJECT1_OCP4_KUBERNETES_NOR_RUNNER_TOKENMAL
  project1-server-docker-priv: $PROJECT1_SERVER_DOCKER_PRIV_RUNNER_TOKEN
```

Instead of storing those tokens one-by-one in an environment variable or inline, the complete dictionary can be stored in one environment variable in YAML format:

```yaml
runner_tokens: $GITLAB_RUNNER_TOKENS
```

#### `gitlab_instances`

This dictionary contains general information and access tokens for the
configured GitLab instances.

The `key` is an abbreviation for the instance that is used in other places in
the configuration and in the automatically generated names of the runners.

The `value` is a dictionary with the following fields:

| Field                 | Type   | Required | Description                                                                        |
|-----------------------|--------|----------|------------------------------------------------------------------------------------|
| `url`                 | string | yes      | GitLab instance URL                                                                |
| `api_token`           | string | yes      | GitLab personal access token with `owner` access on the configured projects/groups |
| `registration_tokens` | dict   | yes      | GitLab runner registration tokens for groups and projects                          |

Example:

```yaml
gitlab_instances:
  com:
    url: https://gitlab.com/
    api_token: $COM_GITLAB_TOKEN_ADMIN_BOT
    registration_tokens:
      external-group: $COM_GITLAB_REGISTRATION_TOKEN
  corp:
    url: https://gitlab.corp.com/
    api_token: $CORP_GITLAB_TOKEN_ADMIN_BOT
    registration_tokens:
      internal-group: $CORP_GITLAB_REGISTRATION_TOKEN
      internal-group/project1: $CORP_GITLAB_REGISTRATION_TOKEN_PROJECT1
```

#### `runner_deployments`

This dictionary contains information about configured gitlab-runner
deployments. One deployment corresponds to one `config.toml`, i.e. one
gitlab-runner process. Each gitlab-runner can have multiple configurations
specified in `runner_configurations`.

The `key` is an abbreviation for the deployment that is used in other places in
the configuration and in the automatically generated names of the runners.

The `value` is a dictionary used for templating the global keys of the
`config.toml` file.

Additionally, it can contain the following fields:

| Field                 | Type     | Required | Description                                                                                                  |
|-----------------------|----------|----------|--------------------------------------------------------------------------------------------------------------|
| `.configfile`         | filepath | (yes)    | Filename for the gitlab-runner configuration, e.g. `config.toml` (conflicts with `.configmap`)               |
| `.configmap`          | filepath | (yes)    | Filename for a Kubernetes ConfigMap, e.g. `configmap.yaml` (conflicts with `.configfile`)                    |
| `.filename`           | filename | (yes)    | Filename within the ConfigMap (only with `.configmap`)                                                       |
| `.metadata`           | string   | no       | Meta data for the ConfigMap (only with `.configmap`)                                                         |
| `.template_overrides` | dict     | no       | Runner configuration entries that will only be applied to this deployment                                    |

Example:

```yaml
runner_deployments:
  .default:
    check_interval: 0
    log_level: info
  .kubernetes:
    .filename: config.toml
    .metadata:
      labels:
        app: $PROJECT_NAME
      name: $PROJECT_NAME
    concurrent: 100
  ocp3:
    .extends: .kubernetes
    .configmap: ocp3-configmap.yaml
    .template_overrides:
      kubernetes:
        namespace: $OPENSHIFT_PROJECT
  ocp4:
    .extends: .kubernetes
    .configmap: ocp4-configmap.yaml
    .template_overrides:
      kubernetes:
        namespace: $OPENSHIFT4_PROJECT
  server:
    .configfile: server-config.toml
    concurrent: 10
```

#### `runner_templates`

This dictionary contains templates for gitlab-runner configurations, i.e. for a
`[runner]` section of a `config.toml`. gitlab-runner process.

The `key` is an abbreviation for the runner name that is used in other places in
the configuration and in the automatically generated names of the runners.

The `value` is a dictionary used for templating the keys of a `[runner]`
section of a `config.toml` file.

Additionally, it can contain the following fields:

| Field              | Type   | Required | Description                                                                                  |
|--------------------|--------|----------|----------------------------------------------------------------------------------------------|
| `description`      | string | no       | Executor description (only for documentation)                                                |
| `rationale`        | string | no       | Executor rationale (only for documentation)                                                  |
| `.tags`            | dict   | no       | Mapping from tag assignment group to list of gitlab-runner tags                              |
| `.cache`           | string | no       | Deployment-all style bucket specification for the distributed runner cache in S3             |
| `.active`          | bool   | no       | Whether the runner is active (default true)                                                  |
| `.access_level`    | string | no       | The access_level of the runner; `not_protected` or `ref_protected` (default `not_protected`) |
| `.locked`          | bool   | no       | Whether the Runner should be locked for current project (default true)                       |
| `.maximum_timeout` | int    | no       | Maximum timeout set when this Runner will handle the job in seconds (default 1 week)         |
| `.run_untagged`    | bool   | no       | Whether the Runner should handle untagged jobs (default false)                               |

Example:

```yaml
runner_templates:
  .default:
    .cache: $BUCKET_RUNNER_CACHE
  .docker:
    executor: docker
    docker:
      tls_verify: false
  .kubernetes:
    executor: kubernetes
    kubernetes:
      privileged: false
  docker-priv:
    .extends: .docker
    .tags:
      default: [image-builder]
      disabled: []
    docker:
      privileged: true
  kubernetes-high:
    .extends: .kubernetes
    .tags:
      default: [build]
      disabled: []
    limit: 4
    kubernetes:
      cpu_request: "20"
      cpu_limit: "20"
      memory_request: 8Gi
      memory_limit: 8Gi
  kubernetes-normal:
    .extends: .kubernetes
    .tags:
      default: [test]
      disabled: []
    limit: 20
    kubernetes:
      cpu_request: "4"
      cpu_limit: "4"
      memory_request: 4Gi
      memory_limit: 4Gi
```

#### `variable_groups`

This dictionary contains environment variables that can be configured as
trigger variables on a GitLab group or project or in the `environment` key for
a gitlab-runner configuration.

The `key` is an abbreviation for the variable group that is used in other places in
the configuration.

The `value` is a dictionary of environment variables.

Example:

```yaml
variable_groups:
  home:
    HOME: /tmp
  group-default:
    DEFAULT_EMAIL: email@example.com
  project1:
    .extends: group-default
    GITLAB_PASSWORD: $GITLAB_PASSWORD
```

#### `runner_configurations`

This dictionary contains groups of runners that are configured with the same
variables. Those runners can be part of multiple gitlab-runner deployments.

The `key` is an abbreviation for the configuration that is used in other places
and in the automatically generated names of the runners.

The `value` is a dictionary with the following fields:

| Field                | Type   | Required | Description                                                                                   |
|----------------------|--------|----------|-----------------------------------------------------------------------------------------------|
| `runner_deployments` | dict   | yes      | Mapping from runner deployments to list of instantiated runner templates                      |
| `variable_group`     | string | no       | Environment variables set in the `environment` field of the corresponding `[runner]` sections |

Example:

```yaml
runner_configurations:
  com:
    runner_deployments:
      ocp3:
        - kubernetes-high
        - kubernetes-normal
      server:
        - docker-priv
    variable_group: home
  corp:
    runner_deployments:
      ocp3:
        - kubernetes-high
        - kubernetes-normal
      server:
        - docker-priv
    variable_group: home
  project1:
    runner_deployments:
      ocp4:
        - kubernetes-normal
      server:
        - docker-priv
    variable_group: home
```

#### `runner_registrations`

This list contains the registration of runner groups with GitLab groups and
projects. Runners can be part of multiple groups or projects at the same time.

Each item in the list is a dictionary with the following fields:

| Field                   | Type | Required | Description                                                    |
|-------------------------|------|----------|----------------------------------------------------------------|
| `runner_configurations` | list | yes      | Runners that should be linked to the GitLab groups or projects |
| `gitlab_groups`         | list | (yes)    | GitLab groups where runners should be registered               |
| `gitlab_projects`       | list | (yes)    | GitLab projects where runners should be registered             |

Example:

```yaml
runner_registrations:
  - runner_configurations: com
    gitlab_groups: com/external-group
  - runner_configurations: corp
    gitlab_groups: corp/internal-group
  - runner_configurations: project1
    gitlab_projects: corp/internal-group/project1
```

#### `gitlab_variables`

This list contains the assignment of variable groups to GitLab groups and
projects. In other words, this list configures the project/group CI/CD variables.

Each item in the list is a dictionary with the following fields:

| Field             | Type | Required | Description                                                                          |
|-------------------|------|----------|--------------------------------------------------------------------------------------|
| `variable_group`  | list | yes      | Variables that should be set in the CI/CD variables of the GitLab groups or projects |
| `gitlab_groups`   | list | (yes)    | GitLab groups where variables should be set                                          |
| `gitlab_projects` | list | (yes)    | GitLab projects where variables should be set                                        |

Example:

```yaml
gitlab_variables:
  - gitlab_groups:
      - corp/internal-group
      - com/external-group
    variable_group: group-default
  - gitlab_projects:
      - corp/internal-group/project1
    variable_group: project1
```

### Preprocessing

The following preprocessing steps are performed (in order) when reading the configuration.

#### Environment variable substitution

Values of the form `$VARIABLE_NAME` are replaced by the value of the named
environment variable.

#### Top-level string values

For all top-level items, the value can also be specified as a string instead of
a dictionary or list. In this case, the value is transparently parsed as YAML.
This allows to store complete dictionaries like the `runner_tokens` in
environment variables.

#### Inheritance

For some dictionaries, basic inheritance and defaults modeled after the
[default](https://docs.gitlab.com/ce/ci/yaml/#global-defaults) and
[extends](https://docs.gitlab.com/ce/ci/yaml/#extends) keywords in
the [GitLab CI/CD job descriptions](https://docs.gitlab.com/ce/ci/yaml) can be
used.

- items with a key starting with a dot (`.`) are not processed, but can be
  used for inheritance
- the `.default` item will be inherited by all other items
- the `.extends` key followed by a name or list of names
  specifies items to inherit from
- if the items are directly used for templating, items with a key starting with
  a dot (`.`) are removed beforehand

This is enabled for `runner_templates`, `runner_deployments` and
`variable_groups`.

## KCIDB Image.

An extra image is built to run the KCIDB module.
This image is used on OpenShift pods, and contains `kcidb` extra dependencies.

`registry.gitlab.com/cki-project/cki-tools/kcidb`
