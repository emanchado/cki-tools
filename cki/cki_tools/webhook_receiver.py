"""Route webhook messages to an amqp queue."""
import collections
import os
import re
from urllib import parse

import flask
import sentry_sdk
from cki_lib import logger
from cki_lib import messagequeue
from cki_lib import misc
from sentry_sdk.integrations.flask import FlaskIntegration

app = flask.Flask(__name__)

if app.env == 'production':
    sentry_sdk.init(
        ca_certs=os.getenv('REQUESTS_CA_BUNDLE'),
        integrations=[FlaskIntegration()]
    )

LOGGER = logger.get_logger('cki.cki_tools.webhook_receiver')

RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST', 'localhost')
RABBITMQ_PORT = int(os.environ.get('RABBITMQ_PORT', '5672'))
RABBITMQ_USER = os.environ.get('RABBITMQ_USER', 'guest')
RABBITMQ_PASSWORD = os.environ.get('RABBITMQ_PASSWORD', 'guest')
RABBITMQ_EXCHANGE = os.environ.get('RABBITMQ_EXCHANGE', 'webhooks')

QUEUE = messagequeue.MessageQueue(RABBITMQ_HOST, RABBITMQ_PORT,
                                  RABBITMQ_USER, RABBITMQ_PASSWORD)

WAITING = collections.deque()


def check_websecret():
    """Check the GitLab websecret token."""
    key = os.environ.get('WEBHOOK_RECEIVER_WEBSECRET')
    if not key:
        flask.abort(404)
    try:
        secret = flask.request.headers["X-Gitlab-Token"]
    except KeyError:
        flask.abort(403, "Permission denied: missing webhook token")

    if secret != key:
        flask.abort(403, "Permission denied: invalid webhook token")


@app.route('/', methods=['POST'])
def webhook():
    """Process a webhook."""
    check_websecret()

    data = flask.request.json
    if 'object_kind' not in data:
        flask.abort(500, 'Missing object_kind')
    object_kind = data['object_kind']
    if 'project' in data:
        web_url = data['project']['web_url']
    else:
        web_url = data['repository']['homepage']
    web_url = parse.urlsplit(web_url)
    topic = re.sub('[./]+', '.',
                   f'{web_url.hostname}/{web_url.path}/{object_kind}')

    enqueue_and_send(data, topic)
    return "OK"


def enqueue_and_send(data, topic):
    """Enqueue message and try to send queue."""
    WAITING.append((data, topic))
    while WAITING:
        data, topic = WAITING.popleft()
        try:
            if misc.is_production():
                LOGGER.info('Sending webhook payload to %s', topic)
                QUEUE.send_message(data, topic, RABBITMQ_EXCHANGE)
            else:
                LOGGER.info('Not sending because not production: %s: %s',
                            topic, data)
        # pylint: disable=broad-except
        except Exception:
            LOGGER.exception('Error during sending message, will be retried')
            WAITING.appendleft((data, topic))
            return
