"""Common cki_tools helpers NOT for external consumption."""
import collections

import boto3
import botocore
from cached_property import cached_property


class Colors:  # pylint: disable=too-few-public-methods
    """ANSI escape sequences for color output."""

    RED = '\033[1;31m'
    GREEN = '\033[1;32m'
    YELLOW = '\033[1;33m'
    END = '\033[0m'


def cprint(color: Colors, *args, **kwargs):
    """Print colored messages."""
    print(*(f'{color}{str(a)}{Colors.END}' for a in args), **kwargs)


BucketSpec = collections.namedtuple('BucketSpec',
                                    ['endpoint', 'access_key', 'secret_key',
                                     'bucket', 'prefix'])


def parse_bucket_spec(bucket_spec) -> BucketSpec:
    """Parse a deployment-all-style bucket specification."""
    endpoint, access_key, secret_key, bucket, bucket_path = bucket_spec.split(
        '|')
    endpoint = endpoint.rstrip('/') or 'http://s3.amazonaws.com'
    bucket = bucket.rstrip('/')
    bucket_path = bucket_path.rstrip('/') + '/' if bucket_path else ''
    return BucketSpec(endpoint, access_key, secret_key, bucket, bucket_path)


class S3Bucket:
    """S3 bucket spec and client."""

    def __init__(self, bucket_spec: BucketSpec):
        """Create a new instance."""
        self.spec = bucket_spec

    @classmethod
    def from_bucket_string(cls, bucket_spec: str) -> 'S3Bucket':
        """Initialize S3Bucket from bucket_spec as a string."""
        spec = parse_bucket_spec(bucket_spec)
        return cls(spec)

    @cached_property
    def client(self) -> botocore.client.BaseClient:
        """Get s3 bucket client."""
        return boto3.client(
            's3',
            aws_access_key_id=self.spec.access_key,
            aws_secret_access_key=self.spec.secret_key,
            endpoint_url=self.spec.endpoint,
        )
