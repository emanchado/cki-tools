"""Misc utility functions for kcidb."""
import os

from botocore.exceptions import ClientError

from cki_lib.logger import get_logger
from cki.cki_tools import _utils

LOGGER = get_logger(__name__)

BUCKET_PUBLIC_SPEC = os.environ.get('BUCKET_PUBLIC_SPEC')
BUCKET_PRIVATE_SPEC = os.environ.get('BUCKET_PRIVATE_SPEC')
BUCKETS = {
    'public': _utils.S3Bucket.from_bucket_string(
        BUCKET_PUBLIC_SPEC) if BUCKET_PUBLIC_SPEC else None,
    'private': _utils.S3Bucket.from_bucket_string(
        BUCKET_PRIVATE_SPEC) if BUCKET_PRIVATE_SPEC else None
}


def upload_file(visibility, artifacts_path, file_name, file_content=None,
                source_path=None):
    """
    Upload file to final storage.

    This method should decide where to put the file, and return the url
    where to access that file.
    """
    LOGGER.debug('Uploading %s', file_name)

    bucket = BUCKETS.get(visibility)
    if not bucket:
        LOGGER.debug('S3 bucket not configured. Skipping upload.')
        return None

    if not (file_content or source_path):
        LOGGER.warning('File "%s" is empty', file_name)
        return None

    file_path = os.path.join(bucket.spec.prefix, artifacts_path,
                             file_name)
    file_url = os.path.join(bucket.spec.endpoint, bucket.spec.bucket,
                            file_path)

    try:
        bucket.client.head_object(Bucket=bucket.spec.bucket,
                                  Key=file_path)
        LOGGER.debug('File %s already exists in remote', file_name)
    except ClientError:
        if file_content:
            bucket.client.put_object(Bucket=bucket.spec.bucket,
                                     Key=file_path, Body=file_content)
        else:
            bucket.client.upload_file(Bucket=bucket.spec.bucket,
                                      Filename=source_path, Key=file_path)

    LOGGER.debug('File correctly uploaded to %s', file_url)
    return file_url
