"""KCIDB Objects."""
import hashlib
import pathlib
import re
from functools import lru_cache
from urllib.parse import urlparse

import dateutil
import gitlab.exceptions
import gitlab.v4.objects
import koji
from cached_property import cached_property
from rcdefinition.rc_data import parse_config_data

from cki.beaker_tools import utils as beaker_utils
from cki.kcidb.utils import upload_file
from cki_lib.logger import get_logger
from cki_lib.misc import strtobool
from cki_lib.session import get_session

LOGGER = get_logger(__name__)
SESSION = get_session(__name__, LOGGER)


class GitlabPipeline:
    """Gitlab Pipeline class with cache."""

    def __init__(self, pipeline):
        self.pipeline = pipeline

    def __getattr__(self, name):
        """Try getting attributes from here, fallback to the Pipeline instance."""
        try:
            return self.__dict__[name]
        except KeyError:
            return getattr(self.pipeline, name)

    @cached_property
    def variables(self):
        """Return dict with Pipeline variables."""
        return {var.key: var.value for var in self.pipeline.variables.list(all=True)}

    @cached_property
    def jobs(self):
        """Return pipeline jobs."""
        return self.pipeline.jobs.list(all=True)

    @lru_cache(maxsize=None)
    def get_job(self, stage, arch=None, status='success'):
        """Return pipeline job matching the requested parameters."""
        for job in self.jobs:
            if job.stage == stage:
                if arch and arch not in job.name:
                    continue
                if status and job.status != status:
                    continue

                return GitlabJob(job)

        return None

    @cached_property
    def project(self):
        """Get pipeline's project."""
        return self.pipeline.manager.gitlab.projects.get(self.pipeline.project_id)

    @cached_property
    def pipeline_data(self):
        self.pipeline.refresh()
        return {
            'id': self.pipeline.id,
            'variables': self.variables,
            'started_at': self.pipeline.started_at,
            'created_at': self.pipeline.created_at,
            'finished_at': self.pipeline.finished_at,
            'duration': self.pipeline.duration,
            'ref': self.pipeline.ref,
            'sha': self.pipeline.sha,
            'project': {
                'id': self.project.id,
                'path_with_namespace': self.project.path_with_namespace,
            },
        }


class GitlabJob:
    """Gitlab Job class with cache."""

    def __init__(self, job):
        self.job = self.as_project_job(job)

    def __getattr__(self, name):
        """Try getting attributes from here, fallback to the Job instance."""
        try:
            return self.__dict__[name]
        except KeyError:
            return getattr(self.job, name)

    @lru_cache(maxsize=None)
    def get_artifact(self, name):
        return self.job.artifact(name)

    @staticmethod
    def as_project_job(job):
        """
        Turn ProjectPipelineJob into ProjectJob.

        ProjectJob does not have the artifact atribute.
        """
        if isinstance(job, gitlab.v4.objects.ProjectPipelineJob):
            project = job.manager.gitlab.projects.get(job.project_id, lazy=True)
            return project.jobs.get(job.id)

        return job


class Job:
    """Base Job class."""

    def __init__(self, pipeline: GitlabPipeline, job: GitlabJob):
        self.pipeline = pipeline
        self.job = job

    @property
    def visibility(self):
        return self.pipeline.variables.get('artifacts_visibility', 'private')

    @cached_property
    def rc(self):  # pylint: disable=invalid-name
        """Return rc file content as a dict."""
        rc_content = self.job.get_artifact('rc')
        return parse_config_data(rc_content)

    @cached_property
    def rc_state(self):
        try:
            return self.rc['state']
        except gitlab.exceptions.GitlabGetError:
            return {}

    def upload_file(self, file_name, file_content=None, source_path=None):
        """
        Call cki.kcidb.utils.upload_file with correct bucket.
        """
        return upload_file(self.visibility, self.artifacts_path, file_name,
                           file_content, source_path)

    @cached_property
    def job_data(self):
        tag = self.rc_state.get('tag')
        self.rc_state.get('tag')
        return {
            'id': self.job.id,
            'name': self.job.name,
            'stage': self.job.stage,
            'started_at': self.job.started_at,
            'created_at': self.job.created_at,
            'finished_at': self.job.finished_at,
            'duration': self.job.duration,
            'test_hash': self.rc_state.get('test_hash'),
            'tag': str(tag) if tag else None,
            'commit_message_title': self.rc_state.get('commit_message_title'),
            'kernel_version': self.rc_state.get('kernel_version'),
        }

    @property
    def artifacts_path(self):
        """Relative path where to put the files in the storage."""
        return NotImplementedError()

    def _render(self):
        """Return object data as json."""
        return NotImplementedError()

    @staticmethod
    def is_empty(value):
        """Check if value is not empty."""
        return (
            value is None or
            value == [] or
            value == {}
        )

    def render(self):
        # pylint: disable=no-member
        """Return object data removing empty keys."""
        data = self._render().copy()
        misc = {
            'job': self.job_data,
            'pipeline': self.pipeline.pipeline_data,
        }
        data.setdefault('misc', {}).update(misc)
        data['origin'] = 'redhat'
        return {
            key: value for key, value in data.items() if not self.is_empty(value)
        }

    @staticmethod
    def timestamp_to_iso(timestamp):
        """Format timestamp as ISO. Adds tzinfo=UTC if no tzinfo present."""
        date = dateutil.parser.parse(timestamp)
        if not date.tzinfo:
            date = date.replace(tzinfo=dateutil.tz.UTC)
        return date.isoformat()


class Revision(Job):
    """KCIDB Revision."""

    resource_name = 'revision'

    @property
    def artifacts_path(self):
        """Relative path where to put the files in the storage."""
        date = dateutil.parser.parse(self.pipeline.created_at)
        return f'{date.year}/{date.month:02d}/{date.day:02d}/{self.pipeline.id}'

    def _render(self):
        """Return the KCIDB compatible data."""
        return {
            'id': self.id,
            'tree_name': self.pipeline.variables['name'],
            'git_repository_url': self.pipeline.variables['git_url'],
            'git_repository_branch': self.pipeline.variables['branch'],
            'git_commit_hash': self.pipeline.variables['commit_hash'],
            'git_commit_name': None,
            'patch_mboxes': self.patch_mboxes,
            'message_id': self.pipeline.variables.get('message_id'),
            'description': None,
            'publishing_time': self.pipeline.created_at,
            'discovery_time': self.pipeline.created_at,
            'contacts': self.contacts,
            'log_url': self.log_url,
            'valid': self.valid,
            'misc': self.misc,
        }

    @property
    def patch_mboxes(self):
        """List of mboxes containing patches applied."""
        return [
             {
                 'url': url,
                 'name': re.search("([^/]+)/?$", urlparse(url).path)[1]
             } for url in self.pipeline.variables.get('patch_urls', '').split()
         ]

    @property
    def contacts(self):
        """List of e-mail addresses of contacts concerned with this revision."""
        return []

    @property
    def log_url(self):
        """The URL of the log file of the attempt to construct this revision."""
        log_url = self.rc_state['mergelog']
        try:
            log_content = self.job.artifact(log_url)
        except gitlab.exceptions.GitlabGetError:
            return None
        return self.upload_file('merge.log', file_content=log_content)

    @property
    def valid(self):
        """True if the pieces could be combined."""
        return self.rc_state['stage_merge'] == 'pass'

    @property
    def misc(self):
        """Miscellaneous extra data about the revision."""
        return {}

    @cached_property
    def id(self):
        """Revision ID."""
        id = self.pipeline.variables["commit_hash"]

        patch_hash_list = []
        for patch_url in self.pipeline.variables.get('patch_urls', '').split():
            patch = SESSION.get(patch_url)
            patch_hash_list.append(
                hashlib.sha256(patch.content).hexdigest()
            )

        if patch_hash_list:
            id += '+' + hashlib.sha256('\n'.join(patch_hash_list).encode('utf-8')).hexdigest()

        return id


class Build(Job):
    """KCIDB Build."""

    resource_name = 'build'

    @property
    def artifacts_path(self):
        """Relative path where to put the files in the storage."""
        return f'{self.revision.artifacts_path}/build_{self.architecture}_{self.id}'

    def _render(self):
        """Return the KCIDB compatible data."""
        return {
            'revision_id': self.revision.id,
            'id': self.id,
            'description': None,
            'start_time': self.job.started_at,
            'duration': self.rc_state.get('build_time'),
            'architecture': self.architecture,
            'command': self.command,
            'compiler': self.rc_state['compiler'],
            'input_files': self.input_files,
            'output_files': self.output_files,
            'config_name': 'fedora',
            'config_url': self.config_url,
            'log_url': self.log_url,
            'valid': self.valid,
            'misc': self.misc,
        }

    @cached_property
    def revision(self):
        """Get the built revision."""
        merge_job = self.pipeline.get_job(stage='merge', status='success')
        if not merge_job:
            raise Exception('No merge job found for this pipeline')

        revision = Revision(self.pipeline, merge_job)
        return revision

    @property
    def architecture(self):
        """Build architecture."""
        return self.rc_state['kernel_arch']

    @property
    def id(self):
        """Build ID."""
        return f'redhat:{self.job.id}'

    @property
    def command(self):
        """Full shell command line used to make the build."""
        return self.rc["build"]["command"]

    @property
    def input_files(self):
        return None

    @property
    def output_files(self):
        files = []
        for file_path in ('tarball_file', 'selftests_file', 'selftests_buildlog'):
            file = self.rc_state.get(file_path)
            if file:
                try:
                    content = self.job.artifact(file)
                except gitlab.exceptions.GitlabGetError:
                    LOGGER.error("Job %i: File not found: %s", self.job.id, file)
                    continue
                file_name = pathlib.Path(file).name
                files.append(
                    {'name': file_name,
                     'url': self.upload_file(file_name, file_content=content)}
                )
        if self.rc_state.get('repo_path'):
            files.append(
                {'name': 'kernel_package_url', 'url': self.rc_state.get('kernel_package_url')}
            )
        return files

    @cached_property
    def log_url(self):
        """The URL of the build log file."""
        log_url = self.rc_state['buildlog']
        log_content = self.job.artifact(log_url)
        return self.upload_file('build.log', file_content=log_content)

    @property
    def valid(self):
        """True if it could be built."""
        return self.rc_state['stage_build'] == 'pass'

    @property
    def config_url(self):
        """The URL of the build configuration file."""
        config_file = self.rc_state.get('config_file')

        if config_file:
            content = self.job.artifact(config_file)
            return self.upload_file('.config', file_content=content)

    @property
    def misc(self):
        """Miscellaneous extra data about the revision."""
        return {}


class Test(Job):
    """KCIDB Test."""

    resource_name = 'test'

    def __init__(self, pipeline, job, test_info):
        super().__init__(pipeline, job)
        self.test_info = test_info

    @property
    def artifacts_path(self):
        """Relative path where to put the files in the storage."""
        return f'{self.build.artifacts_path}/tests'

    def _render(self):
        """Return the KCIDB compatible data."""
        return {
            'build_id': self.build.id,
            'id': self.id,
            'environment': self.environment,
            'path': self.test_info.get('CKI_UNIVERSAL_ID'),
            'description': self.test_info.get('CKI_NAME'),
            'status': self.status,
            'waived': self.waived,
            'start_time': self.start_time,
            'duration': self.duration,
            'output_files': self.output_files,
            'misc': self.misc,
        }

    @property
    def start_time(self):
        """Start start time as ISO."""
        return self.timestamp_to_iso(self.test_info['start_time'])

    @property
    def duration(self):
        """Return duration in seconds."""
        duration = self.test_info.get('duration')

        # Test is still running.
        if 'Time Remaining' in duration:
            return None

        hours, minutes, seconds = re.match(r'(\d+):(\d+):(\d+)', duration).groups()
        return 3600 * int(hours) + 60 * int(minutes) + int(seconds)

    @cached_property
    def build(self):
        """Get the tested build."""
        # Beaker arch is 'arch', UMB results arch is 'test_arch'
        arch = self.test_info.get('arch') or self.test_info.get('test_arch')

        if self.is_debug:
            arch += '_debug'

        # If 'ARTIFACT_URL_{arch}' is present, we're using a build from another pipeline.
        artifacts_url = self.pipeline.variables.get(f'ARTIFACT_URL_{arch}')
        if artifacts_url:
            job_id = int(re.findall(r'jobs/(\d+)/artifacts', artifacts_url)[0])

            job = GitlabJob(self.pipeline.project.jobs.get(job_id))
            pipeline = GitlabPipeline(self.pipeline.project.pipelines.get(job.pipeline['id']))

            return Build(pipeline, job)

        job = self.pipeline.get_job(stage='build', arch=arch, status='success')
        if not job:
            raise Exception(f'Build for {self.test_info["arch"]} not found.')

        return Build(self.pipeline, job)

    @property
    def id(self):
        """ID of the test run."""
        return f'redhat:{self.test_info["task_id"]}'

    @property
    def environment(self):
        """The environment the test ran in."""
        return {'description': self.test_info['system']}

    @property
    def status(self):
        """
        The test status, one of the following.

        ("ERROR", "FAIL", "PASS", "DONE", "SKIP").
        """
        return beaker_utils.decode_test_result(
            self.test_info['status'],
            self.test_info['result'],
            self.test_info['skipped']
        )

    @property
    def output_files(self):
        """A list of test outputs: logs, dumps, etc."""
        output_files = []
        for file in self.test_info['output_files']:
            output_files.append(
                {'name': file['name'],
                 'url': self.upload_file(file['path'], source_path=file['url'])}
            )

        return output_files

    @property
    def waived(self):
        """Is this test waived?. Look for CKI_WAIVED key."""
        return self.test_info.get('CKI_WAIVED') is not None

    @property
    def targeted(self):
        """Is this test targeted?."""
        if not self.rc_state.get('targeted_tests'):
            return False
        list_path = self.rc_state['targeted_tests_list']
        tests = self.job.get_artifact(list_path)
        tests_list = tests.decode().splitlines()

        return self.test_info.get('CKI_NAME') in tests_list

    @property
    def finish_time(self):
        """Test finish_time."""
        finish_time = self.test_info.get('finish_time')

        # Test is still running.
        if not finish_time:
            return None

        return self.timestamp_to_iso(finish_time)

    @property
    def is_debug(self):
        """This test run on a debug kernel."""
        return strtobool(self.rc_state.get('debug_kernel', 'false'))

    @property
    def misc(self):
        """Miscellaneous extra data about the revision."""
        return {
            'debug': self.is_debug,
            'targeted': self.targeted,
            'fetch_url': self.test_info['fetch_url'],
            'beaker': {
                'task_id': self.test_info['task_id'],
                'recipe_id': self.test_info['recipe_id'],
                'finish_time': self.finish_time,
                'retcode': self.rc_state['retcode'],
            }
        }


class BrewJob(Job):
    """Brew job base class."""

    @property
    def revision_id(self):
        """Get fake revision_id."""
        task_hash = hashlib.sha1(str(self.task['id']).encode()).hexdigest()
        return task_hash

    @cached_property
    def task(self):
        """Get brew task info."""
        session = koji.ClientSession(self.pipeline.variables['server_url'])
        return session.getTaskInfo(self.pipeline.variables['brew_task_id'], request=True)


class BrewRevision(BrewJob):
    """Fake revision for Brew Job."""

    resource_name = 'revision'

    def _render(self):
        """Return json data."""
        return {
            'id': self.revision_id,
            'valid': True,
            'contacts': [
                f'{self.pipeline.variables["owner"]}@redhat.com',
            ]
        }


class BrewBuild(BrewJob):
    """Fake build for Brew Job."""

    resource_name = 'build'

    def _render(self):
        """Return json data."""
        return {
            'revision_id': self.revision_id,
            'id': self.id,
            'architecture': self.architecture,
            'start_time': self.start_time,
            'duration': self.duration,
            'valid': True,
        }

    @property
    def is_debug(self):
        """This is a debug kernel."""
        return strtobool(self.rc_state.get('debug_kernel', 'false'))

    @property
    def id(self):
        """Build ID."""
        id = f'redhat:{self.pipeline.variables["brew_task_id"]}_{self.architecture}'
        if self.is_debug:
            id += '_debug'

        return id

    @property
    def architecture(self):
        """Build architecture."""
        arch = self.rc_state['kernel_arch']

        if self.is_debug:
            arch += '_debug'

        return arch

    @property
    def start_time(self):
        """Build start timestamp."""
        return self.timestamp_to_iso(self.task['start_time'])

    @property
    def duration(self):
        """Build duration."""
        return int(self.task['completion_ts'] - self.task['start_ts'])


class BrewTest(BrewJob, Test):
    """Brew Test."""

    @property
    def artifacts_path(self):
        """Relative path where to put the files in the storage."""
        return f'{self.pipeline.id}/{self.rc_state["kernel_arch"]}'

    @property
    def build(self):
        return BrewBuild(self.pipeline, self.job)


class UMBTest(Test):
    """UMB test result."""

    def __init__(self, pipeline, job, test_info):
        super().__init__(pipeline, job, test_info)

        # Replace pipeline with original_pipeline.
        original_pipeline = self.pipeline.project.pipelines.get(
            self.pipeline.variables['original_pipeline_id']
        )
        self.umb_pipeline = self.pipeline
        self.pipeline = GitlabPipeline(original_pipeline)

    def _render(self):
        """Return json data."""
        return {
            'build_id': self.build.id,
            'id': self.id,
            'path': self.test_info['test_name'],
            'description': self.test_info['test_description'],
            'status': self.test_info['test_result'],
            'waived': strtobool(self.test_info['test_waived']),
            'output_files': self.output_files,
            'misc': self.misc,
        }

    @property
    def id(self):
        return f'redhat:{self.umb_pipeline.id}_{self.test_info["test_index"]}'

    @property
    def output_files(self):
        output_files = []
        for file in self.test_info['test_log_url']:
            output_files.append(
                {'name': pathlib.Path(file).name,
                 'url': file}
            )
        return output_files

    @property
    def is_debug(self):
        """This test run on a debug kernel."""
        return self.test_info.get('is_debug', False)

    @property
    def misc(self):
        return {
            'debug': self.is_debug
        }
