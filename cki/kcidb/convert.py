#!/usr/bin/python3
"""Parse pipeline or job and get KCIDB schemed data."""
import argparse
import base64
import json
import logging
import lzma
import os
import pathlib
import sys
import tempfile
import zipfile
from collections import defaultdict

import datawarehouse
import gitlab
import sentry_sdk
from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue
from cki_lib.session import get_session

from . import objects
from ..beaker_tools import process_logs

LOGGER = get_logger('cki.kcidb.convert')
SESSION = get_session('cki.kcidb.convert', LOGGER)

GITLAB_URL = os.environ.get('GITLAB_URL')
GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
DATAWAREHOUSE_URL = os.environ.get('DATAWAREHOUSE_URL')
DATAWAREHOUSE_TOKEN = os.environ.get('DATAWAREHOUSE_TOKEN')

RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST')
RABBITMQ_PORT = int(os.environ.get('RABBITMQ_PORT', '5672'))
RABBITMQ_USER = os.environ.get('RABBITMQ_USER')
RABBITMQ_PASSWORD = os.environ.get('RABBITMQ_PASSWORD')
RABBITMQ_EXCHANGE = os.environ.get('RABBITMQ_EXCHANGE')
RABBITMQ_QUEUE = os.environ.get('RABBITMQ_QUEUE')


def get_tests_from_test_job(job):
    """
    Process test jobs and extract tests.

    Download job artifacts into a temporary directory, parse
    the logs and yield tests' data.
    """
    if not job.rc['state'].get('recipesets'):
        LOGGER.info('No recipesets found for job %i', job.job.id)
        return

    if not job.rc['state'].get('test_tasks_path'):
        LOGGER.error('Job %i has no test_tasks_path key in rc.', job.job.id)
        return

    LOGGER.info('Getting tests from test job.')
    with tempfile.TemporaryDirectory() as tmpdirname:
        artifacts_file = pathlib.Path(f'{tmpdirname}/artifacts.zip')

        # Couldn't do this with pathlib.Path.write_bytes
        with open(artifacts_file.absolute(), 'wb') as file:
            LOGGER.info('Downloading artifacts zip.')
            job.job.artifacts(streamed=True, action=file.write)
            LOGGER.info('Finished downloading.')

        with zipfile.ZipFile(artifacts_file.absolute(), 'r') as zip_file:
            zip_file.extractall(tmpdirname)

        artifacts_file.unlink()

        tasks_path = os.path.join(tmpdirname, job.rc['state']['test_tasks_path'])
        tests_data = process_logs.process(tasks_path)

        for test in tests_data:
            yield test


def get_cki_objects(pipeline, jobs):
    """Process cki jobs and return kcidb objects."""
    if not isinstance(jobs, list):
        jobs = [jobs]

    kcidb_objects = []
    stage_to_kcidb = {
        'merge': objects.Revision,
        'build': objects.Build,
    }

    for job in jobs:
        if job.stage == 'test':
            # One single test job contains multiple tests.
            test_job = objects.Job(pipeline, job)
            for test_data in get_tests_from_test_job(test_job):
                yield objects.Test(pipeline, job, test_data)

        elif job.stage == 'umb-results':
            test_job = objects.Job(pipeline, job)
            test_list = json.loads(
                lzma.decompress(
                    base64.b64decode(test_job.pipeline.variables['data'])
                ).decode()
            )
            for index, test_data in enumerate(test_list):
                test_data['test_index'] = index
                yield objects.UMBTest(pipeline, job, test_data)
        else:
            try:
                obj = stage_to_kcidb[job.stage]
            except KeyError:
                LOGGER.debug('No kcidb equivalent for %s', job.stage)
                continue
            yield obj(pipeline, job)


def get_brew_objects(pipeline, jobs):
    """Process brew jobs and return kcidb objects."""
    if not isinstance(jobs, list):
        jobs = [jobs]

    yield objects.BrewRevision(pipeline, jobs[0])

    for job in jobs:

        if job.stage == 'test':
            yield objects.BrewBuild(pipeline, job)

            # One single test job contains multiple tests.
            test_job = objects.Job(pipeline, job)
            for test_data in get_tests_from_test_job(test_job):
                yield objects.BrewTest(pipeline, job, test_data)


def process_jobs(pipeline, jobs, raise_on_error=True):
    """Process the jobs and return kcidb json."""
    if pipeline.project_id == 2:
        get_objects = get_cki_objects
    elif pipeline.project_id == 16:
        get_objects = get_brew_objects
    else:
        LOGGER.error("Unknown project.")
        sys.exit(1)

    pipeline = objects.GitlabPipeline(pipeline)
    jobs = [objects.GitlabJob(job) for job in jobs]

    processed_data = defaultdict(list)
    for obj in get_objects(pipeline, jobs):
        try:
            data = obj.render()
        except Exception:  # pylint: disable=broad-except
            if raise_on_error:
                raise
            LOGGER.exception('Error parsing %s', obj.job.id)
            continue

        # obj.resource_name needs to be plural in the submitted data.
        processed_data[f'{obj.resource_name}s'].append(data)

    kcidb_data = {'version': {'major': 1, 'minor': 1}}
    kcidb_data.update(processed_data)

    return kcidb_data


def callback(args, gitlab_instance, body):
    """Process one received job."""
    LOGGER.debug('Message Received: %s', body)
    LOGGER.info('Got message for %s - %i', body['project'], body['job_id'])

    project = gitlab_instance.projects.get(body['project'])
    job = project.jobs.get(body['job_id'])
    pipeline = project.pipelines.get(job.pipeline['id'])

    kcidb_objects = process_jobs(pipeline, [job])
    handle_output(args, kcidb_objects)


def process_queue(args):
    """Handle jobs for action=queue argument choice.

    Listen to RabbitMQ queue and process received jobs.
    """
    with gitlab.Gitlab(args.gitlab_url, args.gitlab_token,
                       session=SESSION) as gitlab_instance:
        queue = MessageQueue(
            args.rabbitmq_host, args.rabbitmq_port,
            args.rabbitmq_user, args.rabbitmq_password,
            connection_params={
                'blocked_connection_timeout': 300, 'heartbeat': 600})

        queue.consume_messages(args.rabbitmq_exchange, ['#'],
                               lambda r, b: callback(args, gitlab_instance, b),
                               queue_name=args.rabbitmq_queue,
                               prefetch_count=5)


def process_single(args):
    """
    Handler for action=single argument choice.

    Process given job or pipeline id.
    """
    gitlab_instance = gitlab.Gitlab(args.gitlab_url, args.gitlab_token, session=SESSION)
    project = gitlab_instance.projects.get(args.project)

    if args.kind == 'pipeline':
        pipeline = project.pipelines.get(args.id)
        jobs = pipeline.jobs.list(all=True)
    else:
        job = project.jobs.get(args.id)
        pipeline = project.pipelines.get(job.pipeline['id'])
        jobs = [job]

    kcidb_objects = process_jobs(pipeline, jobs, raise_on_error=False)
    handle_output(args, kcidb_objects)


def handle_output(args, data):
    """Handle generated data according to args."""
    json_objects = json.dumps(data)

    if args.output_file:
        pathlib.Path(args.output_file).write_text(json_objects)

    if args.output_stdout:
        print(json_objects)

    if args.push:
        dw_api = datawarehouse.Datawarehouse(args.datawarehouse_url, args.datawarehouse_token)
        dw_api.kcidb.submit.create(data=data)


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        description='Parse a Pipeline or Job and get KCIDB schemed data.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--gitlab-url', default=GITLAB_URL,
                        help='URL to the Gitlab instance. Defaults to env GITLAB_URL.')
    parser.add_argument('--gitlab-token', default=GITLAB_TOKEN,
                        help='Token for the Gitlab instance. Defaults to env GITLAB_TOKEN.')

    group_output = parser.add_argument_group('Generated data destination.')
    group_output.add_argument(
        '--push', action='store_true', help='Push output data to Datawarehouse.')
    group_output.add_argument(
        '--datawarehouse-url', default=DATAWAREHOUSE_URL,
        help='URL to the Datawarehouse instance. Defaults to env DATAWAREHOUSE_URL.')
    group_output.add_argument(
        '--datawarehouse-token', default=DATAWAREHOUSE_TOKEN,
        help='Token for the Datawarehouse instance. Defaults to env DATAWAREHOUSE_URL.')
    group_output.add_argument('--output-file', help='Save output to a file.')
    group_output.add_argument('--output-stdout', action='store_true', default=False,
                              help='Echo output to stdout.')

    subparser_action = parser.add_subparsers(dest='action', help='Action to perform.')

    parser_queue = subparser_action.add_parser(
        'queue', help='Monitor queue for new finished jobs.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser_queue.add_argument('--rabbitmq-exchange', default=RABBITMQ_EXCHANGE,
                              help='Defaults to env RABBITMQ_EXCHANGE.')
    parser_queue.add_argument('--rabbitmq-queue', default=RABBITMQ_QUEUE,
                              help='Defaults to env RABBITMQ_QUEUE.')
    parser_queue.add_argument('--rabbitmq-host', default=RABBITMQ_HOST,
                              help='Defaults to env RABBITMQ_HOST.')
    parser_queue.add_argument('--rabbitmq-port', type=int, default=RABBITMQ_PORT,
                              help='Defaults to env RABBITMQ_PORT.')
    parser_queue.add_argument('--rabbitmq-user', default=RABBITMQ_USER,
                              help='Defaults to env RABBITMQ_USER.')
    parser_queue.add_argument('--rabbitmq-password', default=RABBITMQ_PASSWORD,
                              help='Defaults to env RABBITMQ_PASSWORD.')

    parser_single = subparser_action.add_parser(
        'single', help='Parse a single job or pipeline.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser_single.add_argument('kind', help='pipeline or job?', choices=('pipeline', 'job'))
    parser_single.add_argument('project', help='Gitlab project. Id or url-encoded path.')
    parser_single.add_argument('id', help='Gitlab pipeline or job id.')

    return parser.parse_args()


def main():
    """CLI Interface."""
    args = parse_args()

    if misc.is_production():
        sentry_sdk.init(ca_certs=os.getenv('REQUESTS_CA_BUNDLE'))

    if not args.gitlab_url or not args.gitlab_token:
        LOGGER.error('Missing Gitlab parameters.')
        sys.exit(1)

    if args.action == 'queue':
        process_queue(args)
    elif args.action == 'single':
        process_single(args)


if __name__ == '__main__':
    main()
