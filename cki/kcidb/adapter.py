"""Convert pipeline rc-file to kcidb data without touching the pipeline."""
import argparse
import json
import hashlib
import os
import pathlib
import re

from kcidb.io.schema import is_valid

from cki.kcidb.utils import upload_file

from rcdefinition.rc_data import SKTData

from cki_lib.logger import get_logger
from cki_lib.session import get_session

from kcidb_wrap import v3

LOGGER = get_logger(__name__)
SESSION = get_session(__name__, LOGGER)


class KCIDBAdapter:
    # pylint: disable=too-many-instance-attributes
    """Convert pipeline data into kcidb data."""

    def upload_file(self, artifacts_path, file_name, file_content=None,
                    source_path=None):
        """Upload a file, if upload is allowed."""
        if not self.upload:
            LOGGER.info('uploading files is disabled')
            return None

        return upload_file(self.visibility, artifacts_path, file_name,
                           file_content=file_content, source_path=source_path)

    @staticmethod
    def craft_kcidb_data(revisions, builds, tests):
        """Craft kcidb data from internal build, revision and test lists.

        Arguments:
            revisions: a list of Revision objects from datadefinition project.
            builds: a list of Build objects from datadefinition project.
            tests: a list of Test objects from datadefinition project.
        Returns:
            dictionary, kcidb data
        """
        kcidb_data = {"version": dict(major=v3.JSON_VERSION_MAJOR,
                                      minor=v3.JSON_VERSION_MINOR),
                      "revisions": revisions, "builds": builds, "tests": tests}
        return kcidb_data

    def __init__(self, args):
        self.args = args
        self.upload = args.upload

        # Gitlab jobid
        self.jobid = os.environ['CI_JOB_ID']
        self.pipeline_id = os.environ['CI_PIPELINE_ID']

        rcfile = pathlib.Path(os.environ['CI_PROJECT_DIR'], 'rc')
        self.data = SKTData.deserialize(rcfile.read_text())

        self.revisions = []
        self.builds = []
        self.tests = []
        self.kcidb_data = None

        # Relative path where to put the files in the storage.
        self.art_url = os.path.join(os.environ['CI_JOB_YMD'],
                                    str(self.pipeline_id))
        self.architecture = self.data.state.kernel_arch
        self.revision_artifacts_path = \
            f'{self.art_url}/{self.revision_id}'

        self.build_artifacts_path = \
            f'{self.art_url}/build_{self.architecture}_{self.build_id}'

        self.visibility = os.environ.get('artifacts_visibility', 'private')

    @property
    def revision_id(self):
        """ID of the built revision."""
        files_content = self._download_patches(self.data.state.patch_data)
        chash = self.patch_contents2hash(files_content)
        return os.environ['commit_hash'] + (f'+{chash}' if chash else '')

    @property
    def build_id(self):
        """"ID of the tested build."""
        return f'redhat:{self.jobid}'

    @staticmethod
    def patch_contents2hash(patches_contents):
        """Hash patches to get revision id."""
        hashes = []
        if not patches_contents:
            return None

        for content in patches_contents:
            hashes.append(hashlib.sha256(content).hexdigest())

        hashes = '\n'.join(hashes) + '\n'

        final_hash = hashlib.sha256(hashes.encode('utf-8')).hexdigest()

        return final_hash

    @staticmethod
    def _download_url(url, params=None):
        LOGGER.info('fetching %s', url)

        params = params if params else {}
        response = SESSION.get(url, stream=True, params=params)
        response.raise_for_status()

        return response.content

    def _download_patches(self, patch_links):
        """Download patches using requests + session."""
        files_content = []

        for url in patch_links:
            content = self._download_url(url)
            files_content.append(content)

        return files_content

    @property
    def config_url(self):
        """The URL of the build configuration file."""
        if self.data.state.config_file:
            return self.upload_file(self.build_artifacts_path, '.config',
                                    source_path=self.data.state.config_file)
        return None

    @property
    def build_log_url(self):
        """The URL of the build log file."""
        if self.data.state.buildlog:
            return self.upload_file(self.build_artifacts_path, 'build.log',
                                    source_path=self.data.state.buildlog)
        return None

    @property
    def rev_log_url(self):
        """The URL of the log file of the attempt to construct revision."""
        if self.data.state.mergelog:
            return self.upload_file(self.revision_artifacts_path, 'merge.log',
                                    source_path=self.data.state.mergelog)
        return None

    def get_revision(self):
        """Add the kernel being tested to builds."""
        # Compute sha256 sums
        # Origin is always redhat
        revision = v3.Revision({'id': self.revision_id, 'origin': 'redhat'})

        # Get some vars from env
        # tree_name: like rhel8, net-next, rdma, mainline ...
        revision.tree_name = os.environ['tree_name']
        # git url of sources
        revision.git_repository_url = os.environ['git_url']
        # This doesn't take into account patches applied on top
        revision.git_commit_hash = os.environ['commit_hash']

        revision.git_commit_name = self.data.revision.git_commit_name
        # git source branch
        revision.git_repository_branch = os.environ['branch']
        # patches applied on top
        revision.patch_mboxes = [{'name': re.fullmatch('.*/(.*?)/mbox/?',
                                                       url).group(1),
                                  'url': url} for url in
                                 self.data.state.patch_data]

        revision.message_id = os.environ.get('message_id', None)

        revision.description = self.data.state.patch_subjects[0] if \
            self.data.state.patch_subjects \
            else self.data.revision.description
        revision.publishing_time = self.data.revision.publishing_time
        revision.discovery_time = os.environ['discovery_time']
        # Intentionally left blank for now
        # TODO: determine if kpet-db info should be put here
        revision.contacts = []
        # We don't push invalid data yet.
        revision.valid = True
        revision.misc = {"pipeline_id": self.pipeline_id}

        # Whenever this is None, the element will not be rendered.
        revision.log_url = self.rev_log_url

        return revision

    def get_build(self):
        """Construct build kcidb data."""
        build = v3.Build({'id': self.build_id, 'origin': 'redhat',
                          'revision_id': self.revision_id})

        with_data = f' with {self.data.state.patch_subjects[0]} patches' \
            if self.data.state.patch_subjects else ''
        build.description = f"CKI build of " \
                            f"{os.environ['cki_pipeline_branch']}" + with_data

        build.start_time = self.data.build.start_time
        build.duration = self.data.build.duration
        build.architecture = self.data.state.kernel_arch
        build.command = self.data.build.command
        build.compiler = self.data.state.compiler

        build.input_files = []
        if self.config_url:
            build.config_url = self.config_url
        build.output_files = self.build_output_files
        build.config_name = os.environ.get('config_target', '')
        build.valid = True
        build.misc = {"pipeline_id": self.pipeline_id,
                      "job_id": self.jobid}

        # Whenever this is None, the element will not be rendered.
        build.log_url = self.build_log_url

        return build

    @property
    def build_output_files(self):
        """Construct urls to build output files and upload them."""
        files = []
        state = self.data.state
        for fpath in (state.tarball_file, state.selftests_file,
                      state.selftests_buildlog):
            if fpath:
                fpath_obj = pathlib.Path(fpath)
                if not fpath_obj.is_file():
                    LOGGER.error("Job %i: File not found: %s", self.jobid,
                                 fpath)
                    continue
                file_name = fpath_obj.name
                url = self.upload_file(file_name,
                                       self.build_artifacts_path,
                                       'merge.log',
                                       source_path=self.build_log_url)
                if url:
                    files.append({'name': file_name, 'url': url})
        if self.data.state.repo_path:
            files.append({'name': 'kernel_package_url',
                          'url': self.data.state.kernel_package_url})
        return files

    def output_current_build(self):
        """Dump kcidb build data."""
        kcidb_data = self.craft_kcidb_data([], [self.get_build().to_mapping()], [])

        self.dump(kcidb_data, os.environ['BUILD_PATH'])

    @staticmethod
    def dump(kcidb_data, to_where):
        """Dump kcidb_data to a file."""
        assert is_valid(kcidb_data)

        with open(to_where, 'w') as fhandle:
            json.dump(kcidb_data, fhandle)

    def output_current_revision(self):
        """Dump kcidb revision data."""
        kcidb_data = self.craft_kcidb_data([self.get_revision().to_mapping()], [], [])

        self.dump(kcidb_data, os.environ['REVISION_PATH'])


def main():
    """Convert and dump data."""

    parser = argparse.ArgumentParser(
        description='Dump pipeline data as kcidb json.')
    parser.add_argument('artifact',
                        choices=['build', 'revision'],
                        help='"build" or "revision". The script will dump'
                             'data into $BUILD_PATH or $REVISION_PATH'
                             'respectively.')

    parser.add_argument('--no-upload', default=True, dest='upload',
                        action='store_false',
                        help="By default, files are uploaded. Use --no-upload"
                             "to prevent this behavior.")

    args = parser.parse_args()

    adapter = KCIDBAdapter(args)
    if args.artifact == 'build':
        adapter.output_current_build()
    elif args.artifact == 'revision':
        adapter.output_current_revision()
    else:
        raise RuntimeError('invalid cmd-line args!')


if __name__ == '__main__':
    main()
