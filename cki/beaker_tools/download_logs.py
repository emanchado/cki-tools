#! /usr/bin/python3
"""
Script to download logs from beaker recipesets.

This is a temporary script that download all the logs to process them on
the pipeline. It should not be necessary once the Restraint runner is
merged into the pipeline.
"""
import argparse
import logging
import os
import pathlib
import time
import xml.etree.ElementTree as ET
from concurrent.futures import ThreadPoolExecutor, as_completed

from rcdefinition.rc_data import parse_config_data

from cki_lib.logger import get_logger
from cki_lib.session import get_session

LOGGER = get_logger('cki.beaker_tools.download_logs')
SESSION = get_session('cki.beaker_tools.download_logs')

DOWNLOAD_WORKERS = 20

BEAKER_URL = os.environ.get('BEAKER_URL')


class Beaker:
    def __init__(self, url):
        self.url = url

    def get_recipe_ids(self, recipeset_id):
        """Get a list of recipe_id from a recipeset."""
        url = f"{self.url}/recipesets/{recipeset_id}"
        response = SESSION.get(url)
        recipes = response.json()['machine_recipes']

        return [recipe['id'] for recipe in recipes]

    def get_recipe_xml(self, recipe_id, save_dir=None):
        url = f"{self.url}/recipes/{recipe_id}.xml"
        response = SESSION.get(url)

        if save_dir:
            path = f'{save_dir.rstrip("/")}/{recipe_id}.xml'
            pathlib.Path(os.path.dirname(path)).mkdir(parents=True, exist_ok=True)
            pathlib.Path(path).write_bytes(response.content)

        return ET.fromstring(response.content)


class File:
    def __init__(self, url, destination_path):
        self.url = url
        self.destination_path = destination_path
        self.content = None

    def download(self, dry_run=False):
        """Download. Handles retries."""
        LOGGER.debug('Downloading %s', self.url)

        if dry_run:
            return

        response = SESSION.get(self.url)
        self.content = response.content

    def save(self, dry_run=False):
        """Save file."""
        LOGGER.debug('Saving %s on %s', self.url, self.path)

        if dry_run:
            return

        pathlib.Path(self.directory).mkdir(parents=True, exist_ok=True)
        pathlib.Path(self.path).write_bytes(self.content)

    @property
    def path(self):
        return os.path.join(self.destination_path, self.url.lstrip(BEAKER_URL))

    @property
    def directory(self):
        return os.path.dirname(self.path)

    def __str__(self):
        return self.url


def process_rc(rc_data, destination_path, dry_run=False):
    """Process RC file and download the test logs."""
    beaker = Beaker(BEAKER_URL)
    xml_destination = destination_path
    logs_destination = os.path.join(destination_path, 'recipes')

    rc = parse_config_data(rc_data)
    recipesets = rc['state'].get('recipesets', '').split()

    if not recipesets:
        LOGGER.error("No recipesets found")
        return

    with ThreadPoolExecutor(max_workers=DOWNLOAD_WORKERS) as executor:
        future_to_file = {}

        for recipeset in recipesets:
            recipeset_id = recipeset.split(':')[1]
            recipe_ids = beaker.get_recipe_ids(recipeset_id)
            for recipe_id in recipe_ids:
                recipe_xml = beaker.get_recipe_xml(
                    recipe_id, save_dir=xml_destination if not dry_run else None
                )
                for log in recipe_xml.iter('log'):
                    file = File(log.get('href'), logs_destination)
                    future_to_file[executor.submit(file.download, dry_run)] = file

        for future in as_completed(future_to_file):
            file = future_to_file[future]
            try:
                future.result()
                file.save(dry_run=dry_run)
            except Exception as exc:
                LOGGER.error('Failed: %s - %s', exc, file)


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--rc', help="rc file path.", default="rc")
    parser.add_argument('--destination-path', help="Where to store the logs.", default="beaker_logs")
    parser.add_argument('--dry-run', action='store_true', help='Don\'t download files. Just print the file names.')

    return parser.parse_args()


if __name__ == '__main__':
    arguments = parse_args()

    rc_data = pathlib.Path(arguments.rc).read_text()

    if arguments.dry_run:
        LOGGER.warning('DRY RUN. Will not download the files.')

    process_rc(rc_data, arguments.destination_path, dry_run=arguments.dry_run)
