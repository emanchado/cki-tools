#! /usr/bin/python3
import argparse
import glob
import json
import logging
import os
import pathlib
import re
import xml.etree.ElementTree as ET

from cached_property import cached_property

from cki_lib.logger import get_logger

LOGGER = get_logger('cki.beaker_tools.process_logs')


def glob_files(path):
    """
    Get all files matching with regex, recursively.

    Like glob.iglob() but filtering out directories and sorted.
    """
    return sorted(f for f in glob.iglob(path, recursive=True)
                  if os.path.isfile(f))


def sanitize_string(string):
    """Remove non alphanumperic chars from a string."""
    return re.sub(r'[^a-zA-Z\d]+', '_', string)


def clean_empty_dirs(src_dir):
    """Remove empty directories."""
    for dirpath, _, _ in os.walk(src_dir, topdown=False):
        if dirpath == src_dir:
            break
        try:
            os.rmdir(dirpath)
        except OSError:
            pass


class File:
    def __init__(self, path, parent):
        self._path = pathlib.Path(path)
        self.parent = parent

    def read(self):
        """Read file."""
        try:
            return self._path.read_text(errors='ignore')
        except FileNotFoundError:
            return ''

    def write(self, content, append=False):
        """Write to file."""
        if not self._path.is_file():
            pathlib.Path(os.path.dirname(self.path)).mkdir(parents=True, exist_ok=True)

        if append:
            content = self.read() + content

        return self._path.write_text(content)

    def delete(self):
        """Delete file."""
        self._path.unlink()

    @property
    def name(self):
        """Get file name."""
        return self._path.name

    @property
    def path(self):
        """Get path of the file."""
        return str(self._path.absolute())

    def __lt__(self, other):
        return self.path < other.path

    def __eq__(self, other):
        return self.path == other.path

    def move(self, dest):
        """Move file to dest."""
        pathlib.Path(os.path.dirname(dest)).mkdir(parents=True, exist_ok=True)
        new_path = pathlib.Path(dest)
        self._path.rename(new_path)
        self._path = new_path

    @property
    def recipe(self):
        """Get file's recipe."""
        if isinstance(self.parent, BeakerTask):
            return self.parent.recipe
        if isinstance(self.parent, BeakerRecipe):
            return self.parent
        raise Exception('Can\'t get recipe from unknown parent.')

    @cached_property
    def friendly_path(self):
        """Generate index-human-friendy path."""
        pre = ''
        if isinstance(self.parent, BeakerTask):
            # Prepend test name as folder.
            pre = sanitize_string(self.parent.parameters["CKI_NAME"]) + '/'

        arch = self.recipe.parameters['arch']
        host_id = sorted(list(self.recipe.recipeset.recipes.keys())).index(self.recipe.id) + 1

        return f'{pre}{self.recipe.id}_{arch}_{host_id}_{self.name}'

    def move_to_friendly_path(self):
        """Move the file to self.friendly_path."""
        new_path = os.path.join(
            self.recipe.recipeset.path,
            self.friendly_path
        )
        self.move(new_path)

    def sanitize(self):
        """If necessary, clean unnecessary lines."""
        if self.name.endswith('console.log'):
            try:
                kernel_version = self.parent.parameters['kernel_version']
            except KeyError:
                LOGGER.warning('Kernel version missing. Cannot sanitize console.log')
                return
            kernel_banner = f'Linux version {kernel_version}'
            regex = f'{re.escape(kernel_banner)}.*'
            matches = re.compile(regex).split(self.read())

            if len(matches) > 1:
                sanitized = kernel_banner + ''.join(matches[1:])
            else:
                sanitized = ''

            self.write(sanitized)

    @classmethod
    def sanitized(cls, path, parent):
        log = cls(path, parent)
        log.sanitize()
        return log


class BeakerTask:
    def __init__(self, recipe, task_id):
        self.id = task_id
        self.recipe = recipe
        self.path = f'{recipe.path}/tasks/{task_id}'
        self.parameters = {}

    @cached_property
    def logs(self):
        """Get list of logs from globbing the path."""
        return [File.sanitized(path, self) for path in glob_files(f'{self.path}/**/*')]

    @property
    def all_logs(self):
        """Get logs from task and recipe together.."""
        return self.logs + self.recipe.logs

    def merge_result_logs(self):
        """Merge split file logs into a single one."""
        unique_filenames = {log.name for log in self.logs}
        for unique_filename in sorted(unique_filenames):
            repeated_files = [
                log for log in self.logs if log.name == unique_filename
            ]

            if len(repeated_files) == 1:
                # Not repeated.
                continue

            # Generate a new name into results/ dir without the Job id.
            new_file = File(
                re.sub(
                    pattern=r'(\d+/tasks/\d+/results/)(\d+/logs/)(.*)',
                    repl='\\1\\3',
                    string=repeated_files[0].path
                ),
                self
            )

            if new_file not in self.logs:
                # If the new_file is already on the logs it means that the regex
                # replacement didnt affect the path. Don't add it to the list
                # of logs in order to avoid duplicated items.
                self.logs.append(new_file)

            for file in sorted(repeated_files):
                if file.path == new_file.path:
                    continue
                new_file.write(file.read(), append=True)
                file.delete()
                self.logs.remove(file)

    def aggregate_from_beaker_xml(self, xml_data):
        """Add beaker data from task xml."""
        for param in xml_data.iter('param'):
            key, value = param.get('name'), param.get('value')
            if not key.startswith('CKI'):
                continue
            self.parameters[key] = value

        task_results = list(xml_data.iter('result'))

        # Tasks with path '/' are system events as External Watchdog Expired
        # and tests purposely aborted by the script (i.e. failed while fetching
        # external resources or other handled errors).
        # These tasks did not run or finish correctly.
        # Exclude result.result == 'Panic'. These are real ☠️..
        skipped = all([
            result.get('path') == '/' and result.get('result') != 'Panic'
            for result in task_results
        ])

        fetch = xml_data.find('fetch')

        self.parameters.update({
            'start_time': xml_data.get('start_time'),
            'finish_time': xml_data.get('finish_time'),
            'duration': xml_data.get('duration'),
            'result': xml_data.get('result'),
            'status': xml_data.get('status'),
            # fetch needs to be compared to None, as it *always* will test false.
            # https://docs.python.org/3.10/library/xml.etree.elementtree.html#element-objects
            'fetch_url': fetch.get('url') if fetch is not None else None,
            'skipped': skipped,
        })

        if not self.parameters.get('CKI_NAME'):
            # Not a task generated by us.
            self.delete()

    def delete(self):
        """Delete instance and unlink from recipe."""
        del self.recipe.tasks[self.id]
        for log in self.logs:
            log.delete()

    def make_file_tree_human_friendly(self):
        """Order files to a more human friendly tree structure."""
        self.merge_result_logs()
        for log in self.logs:
            log.move_to_friendly_path()


class BeakerRecipe:
    def __init__(self, recipeset, recipe_id):
        self.id = int(recipe_id)
        self.recipeset = recipeset
        self.path = f'{self.recipeset.path}/{recipe_id}'
        self.parameters = {}

    @cached_property
    def logs(self):
        """Get list of logs from globbing the path."""
        return [
            File.sanitized(path, self) for path in glob_files(f'{self.path}/logs/*')
        ]

    @cached_property
    def tasks(self):
        """Return child tasks."""
        if not os.path.isdir(f'{self.path}/tasks'):
            return {}
        return {
            int(task_id): BeakerTask(self, int(task_id)) for task_id in sorted(os.listdir(f'{self.path}/tasks'))
        }

    def aggregate_from_beaker_xml(self, xml_data):
        """Add beaker data from recipe xml."""
        recipe = xml_data.find('recipeSet/recipe')
        self.parameters = {
            'arch': recipe.get('arch'),
            'system': recipe.get('system'),
            'kernel_version': re.findall(r'cki@gitlab:\d+\s([^@]+)@.*', xml_data.find('whiteboard').text)[0]
        }
        for task in xml_data.iter('task'):
            task_id = int(task.get('id'))
            try:
                self.tasks[task_id].aggregate_from_beaker_xml(task)
            except KeyError:
                LOGGER.warning('R:%i T:%i not found in local tasks.', self.id, task_id)

    def make_file_tree_human_friendly(self):
        """Order files to a more human friendly tree structure."""
        for log in self.logs:
            log.move_to_friendly_path()
        for task in self.tasks.values():
            task.make_file_tree_human_friendly()


class BeakerRecipeSet:
    def __init__(self, path):
        self.base_path = path
        self.path = f'{self.base_path}/recipes'

    @cached_property
    def recipes(self):
        """Return child recipes."""
        return {
            int(recipe_id): BeakerRecipe(self, int(recipe_id)) for recipe_id in sorted(os.listdir(self.path))
        }

    def aggregate_from_beaker(self):
        """Query beaker and add the retrieved data to the recipes and tasks."""
        for recipe in self.recipes.values():
            content = pathlib.Path(f'{self.base_path}/{recipe.id}.xml').read_bytes()
            root = ET.fromstring(content)
            recipe.aggregate_from_beaker_xml(root)

    def make_file_tree_human_friendly(self):
        """Order files to a more human friendly tree structure."""
        for recipe in self.recipes.values():
            recipe.make_file_tree_human_friendly()
        clean_empty_dirs(self.path)

    def dump(self):
        """Dump data into a parseable style."""
        tests = []
        for recipe in self.recipes.values():
            for task in recipe.tasks.values():
                if not task.parameters.get('CKI_NAME'):
                    continue

                test = {
                    'task_id': task.id,
                    'recipe_id': recipe.id,
                    'output_files': [
                        {'name': log.name, 'url': log.path, 'path': log.friendly_path}
                        for log in task.all_logs
                    ]
                }

                test.update(task.parameters)
                test.update(recipe.parameters)
                tests.append(test)

        return tests


def process(source_path):
    brs = BeakerRecipeSet(source_path)
    brs.aggregate_from_beaker()
    brs.make_file_tree_human_friendly()

    return brs.dump()


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        description='From a beaker-styled tree of logs, order and clean the files and generate good metadata.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--source-path', help="Where the logs are stored.", default="beaker_logs")
    parser.add_argument('--output-path', help="Where to output the generated dump file.", default="tests.json")

    return parser.parse_args()


if __name__ == '__main__':
    arguments = parse_args()

    data = process(arguments.source_path)

    # Dump the data into output_file.
    pathlib.Path(arguments.output_path).write_text(
        json.dumps(data)
    )
