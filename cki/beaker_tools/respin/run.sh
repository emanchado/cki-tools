#!/bin/bash

set -euo pipefail

cd /code

/usr/bin/redis-server /etc/redis.conf &
echo "PID redis: $!"

celery -A cki.beaker_tools.respin.worker worker --concurrency=1 &
echo "PID worker: $!"

gunicorn --bind 0.0.0.0:8000 -k sync --reload cki.beaker_tools.respin.webhook:API &
echo "PID webhook: $!"

# Tell the whole process group that it's time to quit.
trap 'trap - SIGINT SIGTERM EXIT && kill -- -$$' SIGINT SIGTERM EXIT

wait
