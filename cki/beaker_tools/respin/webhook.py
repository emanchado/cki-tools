#!/usr/bin/env python3
# Copyright (c) 2018 - 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Webhook receiver for respin-service."""
import json
import os
from importlib import resources

import celery.result
import falcon

from . import worker


# pylint: disable=too-few-public-methods,unused-argument
class ServeRespin():
    """Serve the respin interface."""

    @staticmethod
    def on_get(req, resp):
        """Return respin.html script and HTTP 200."""
        resp.status = falcon.HTTP_200
        resp.content_type = 'text/html'
        resp.body = resources.read_binary(__package__, 'respin.html')

    @staticmethod
    def on_post(req, resp):
        """Trigger Beaker."""
        args = json.load(req.stream) if req.content_length else []
        resp.status = falcon.HTTP_200
        resp.content_type = 'text/plain'
        resp.body = worker.respin_beaker.delay(args).id


# pylint: disable=too-few-public-methods,unused-argument
class ServeStatus():
    """Return task status."""

    @staticmethod
    def on_get(req, resp, task):
        """Return respin.html script and HTTP 200."""
        result = celery.result.AsyncResult(task)
        if not result.ready():
            resp.status = falcon.HTTP_204  # no content
            return
        if result.successful():
            resp.status = falcon.HTTP_200
            resp.body = result.result.replace(
                'J:', f'{os.environ["BEAKER_URL"]}/jobs/')
        else:
            resp.status = falcon.HTTP_500
            resp.body = str(result.result)
        resp.content_type = 'text/plain'


# Set up Falcon's API and enable parsing of form data.
API = falcon.API()
API.req_options.auto_parse_form_urlencoded = False

API.add_route('/respin', ServeRespin())
API.add_route('/status/{task}', ServeStatus())
