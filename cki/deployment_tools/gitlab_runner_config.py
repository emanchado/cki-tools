#!/usr/bin/python3
"""Manage GitLab runners and project variables."""
import argparse
import contextlib
import copy
import difflib
import os
import pathlib
import re
import subprocess
import sys
import tempfile
from urllib import parse

import toml
import yaml
from cached_property import cached_property
from gitlab import Gitlab

from cki.cki_tools import _utils
from pipeline_tools import utils as pt_utils

from cki_lib.logger import get_logger
from cki_lib.session import get_session

LOGGER = get_logger('cki.deployment_tools.gitlab_runner_config')
SESSION = get_session('cki.deployment_tools.gitlab_runner_config', LOGGER)


# pylint: disable=too-many-ancestors
class BlockDumper(yaml.SafeDumper):
    """Block-style literals for strings with newlines."""

    def represent_str(self, data):
        """Format strings with newlines as block-style literals."""
        if '\n' not in data:
            return super().represent_str(data)
        return self.represent_scalar(u'tag:yaml.org,2002:str', data, style='|')


yaml.add_representer(str, BlockDumper.represent_str, Dumper=BlockDumper)


# pylint: disable=too-many-instance-attributes
class RunnerConfig:
    """Manage GitLab runners and project variables."""

    def __init__(self, config, args):
        """Initialize and load the config."""
        self.args = args
        self.config = self._resolve_variables(config)
        self.runner_tokens = self._config('runner_tokens', {})
        self.gitlab_instances = self._config('gitlab_instances', {})
        self.runner_templates = self._config('runner_templates', {}, True)
        self.runner_deployments = self._config('runner_deployments', {}, True)
        self.variable_groups = self._config('variable_groups', {}, True)
        self.runner_configurations = self._config('runner_configurations', {})
        self.runner_registrations = self._config('runner_registrations', [])
        self.gitlab_variables = self._config('gitlab_variables', {})
        self.token_not_found = False
        self._group_cache = {}
        self._project_cache = {}

    @staticmethod
    @contextlib.contextmanager
    def _tempfile(prefix, contents):
        """Create a temporary yaml file and return the name."""
        with tempfile.NamedTemporaryFile(mode='w+t',
                                         prefix=prefix,
                                         suffix='.yaml') as temp:
            temp.write(contents)
            temp.flush()
            yield temp.name

    @staticmethod
    def _yaml_dump(what, **kwargs):
        """Dump data to a yaml file with the BlockDumper."""
        return yaml.dump(what, Dumper=BlockDumper, **kwargs)

    @staticmethod
    def _sorted(value):
        """Recursivly sort a data structure."""
        if isinstance(value, dict):
            return {k: RunnerConfig._sorted(v)
                    for k, v in sorted(value.items())}
        if isinstance(value, list):
            return [RunnerConfig._sorted(v) for v in value]
        return value

    @staticmethod
    def _list(dictionary, key):
        """Return list or a one-element list for single strings."""
        result = dictionary.get(key, [])
        if isinstance(result, str):
            result = [result]
        return result

    @staticmethod
    def _resolve_variables(value):
        """Resolve env variables $NAME throughout in a data structure."""
        if isinstance(value, dict):
            return {k: RunnerConfig._resolve_variables(v)
                    for k, v in value.items()}
        if isinstance(value, list):
            return [RunnerConfig._resolve_variables(v) for v in value]
        if isinstance(value, str) and '$' in value:
            return re.sub(r'\$(\w+|\{([^}]*)\})', lambda m:
                          os.environ[m.group(2) or m.group(1)], value)
        return value

    @staticmethod
    def _gitlab_variables(gl_project_group):
        """Return a dict of project/group CI/CD variables."""
        return {v.key: v.value
                for v in gl_project_group.variables.list(as_list=False)}

    def _config(self, name, default, tree=False):
        """Return a dict or list, or parse a string as yaml."""
        value = self.config.get(name, default)
        if isinstance(value, str):
            value = yaml.safe_load(value)
        if tree:
            value = pt_utils.process_config_tree(value)
        return value

    def _gitlab_runner(self, instance_name, runner_id):
        """Return a dict describing a gitlab runner config."""
        gl_instance = self._gl_instance(instance_name)
        gl_runner = gl_instance.runners.get(runner_id)
        runner = {
            '.groups': [],
            '.projects': [],
            '.id': gl_runner.id,
            'description': gl_runner.description,
            'access_level': gl_runner.access_level,
            'active': gl_runner.active,
            'locked': gl_runner.locked,
            'maximum_timeout': gl_runner.maximum_timeout,
            'run_untagged': gl_runner.run_untagged,
            'tag_list': sorted(gl_runner.tag_list),
        }
        for group in gl_runner.groups:
            gid = group['id']
            if gid not in self._group_cache:
                self._group_cache[gid] = gl_instance.groups.get(gid)
            path = self._group_cache[gid].full_path
            runner['.groups'].append(f'{instance_name}/{path}')
        for project in gl_runner.projects:
            pid = project['id']
            if pid not in self._project_cache:
                self._project_cache[pid] = gl_instance.projects.get(pid)
            path = self._project_cache[pid].path_with_namespace
            runner['.projects'].append(f'{instance_name}/{path}')
        return runner

    @cached_property
    def _gitlab_runners(self):
        """Return a dict describing all accessible gitlab runners."""
        runners = {}
        for instance_name in self.gitlab_instances:
            gl_instance = self._gl_instance(instance_name)
            for gl_runner in gl_instance.runners.list(as_list=False):
                print(f'Getting details for {gl_runner.description}',
                      file=sys.stderr)
                runner = self._gitlab_runner(instance_name, gl_runner.id)
                runners[gl_runner.description] = runner
        return runners

    def _gl_instance(self, instance_name):
        """Return a Gitlab instance for an instance abbreviation."""
        config = self.gitlab_instances[instance_name]
        return Gitlab(config['url'], config['api_token'], session=SESSION)

    def _gl_group(self, group_name, lazy=True):
        """Return a Gitlab instance for an instance/group."""
        instance_name, group = group_name.split('/', 1)
        gl_instance = self._gl_instance(instance_name)
        return gl_instance.groups.get(group, lazy=lazy)

    def _gl_project(self, project_name, lazy=True):
        """Return a Gitlab instance for an instance/project."""
        instance_name, project = project_name.split('/', 1)
        gl_instance = self._gl_instance(instance_name)
        return gl_instance.projects.get(project, lazy=lazy)

    def _registration_data(self, project_group_names):
        """Try to find instance+registration token for a list of projects."""
        for project_group_name in project_group_names:
            instance, project_group = project_group_name.split('/', 1)
            config = self.gitlab_instances[instance]
            token = config.get('registration_tokens', {}).get(project_group)
            if token:
                return {
                    'instance': instance,
                    'token': token
                }
        return None

    @staticmethod
    def _get_instance_name(name, project_group_names):
        """Return the common instance abbreviation for a list of projects."""
        instance_names = [p.split('/', 1)[0] for p in project_group_names]
        if not instance_names:
            raise Exception(f'Cannot find instance for {name}')
        if any(instance_names[0] != u for u in instance_names):
            raise Exception(f'Multiple instances for {name}')
        return instance_names[0]

    def _get_instance_url(self, configuration_name):
        """Return the common instance url for a configuration."""
        project_group_names = []
        for registration in self.runner_registrations:
            if configuration_name not in self._list(
                    registration, 'runner_configurations'):
                continue
            project_group_names += self._list(registration, 'gitlab_projects')
            project_group_names += self._list(registration, 'gitlab_groups')
        return self.gitlab_instances[self._get_instance_name(
            configuration_name, project_group_names)]['url']

    @staticmethod
    def _get_cache_section(bucket_spec, configuration_name):
        """Return the cache section of a gitlab-runner config file."""
        spec = _utils.parse_bucket_spec(bucket_spec)
        endpoint = parse.urlparse(spec.endpoint)
        return {
            'Type': 's3',
            'Shared': True,
            'Path': spec.prefix + configuration_name,
            's3': {
                'ServerAddress': endpoint.hostname,
                'AccessKey': spec.access_key,
                'SecretKey': spec.secret_key,
                'BucketName': spec.bucket,
                'Insecure': endpoint.scheme == 'http',
            }}

    def _configurations_runners(self, configuration_names):
        """Return all runners for a list of configuration."""
        runners = []
        for configuration_name in configuration_names:
            runners += self._configuration_runners(configuration_name)
        return runners

    def _configuration_runners(self, configuration_name):
        """Return all runners for a configuration."""
        configuration = self.runner_configurations[configuration_name]
        runner_deployments = configuration['runner_deployments']
        runners = []
        for deployment_name in runner_deployments:
            runners += self._deployment_runners(
                configuration_name, deployment_name,
                self._list(runner_deployments, deployment_name))
        return runners

    def _deployment_runners(self, configuration_name, deployment_name,
                            runner_template_names):
        """Return all runners for a deployment of a configuration."""
        runners = []
        configuration = self.runner_configurations[configuration_name]
        deployment = self.runner_deployments[deployment_name]
        for runner_name in runner_template_names:
            runner_config = copy.deepcopy(self.runner_templates[runner_name])
            pt_utils.merge_dicts(runner_config,
                                 deployment.get('.template_overrides', {}))
            runner_config['.deployment_name'] = deployment_name
            cache = runner_config.get('.cache')
            if cache:
                runner_config['cache'] = self._get_cache_section(
                    cache, configuration_name)
            variable_group = configuration.get('variable_group')
            if variable_group:
                variables = self.variable_groups[variable_group].items()
                runner_config['environment'] = ['='.join(v) for v in variables]
            runner_config['url'] = self._get_instance_url(configuration_name)
            fullname = f'{configuration_name}-{deployment_name}-{runner_name}'
            runner_config['name'] = fullname
            if fullname in self.runner_tokens:
                runner_config['token'] = self.runner_tokens[fullname]
            else:
                print(f'Runner token for {fullname} not found!',
                      file=sys.stderr)
                self.token_not_found = True
            runners.append(runner_config)
        return runners

    @staticmethod
    def _load_config(config):
        """Load a gitlab-runner config file from a string."""
        return toml.loads(config)

    def _load_configmap(self, filename):
        """Load a gitlab-runner config file from a ConfigMap if possible."""
        path = pathlib.Path(self.args.directory, filename)
        if not path.exists():
            return {}
        configmap = yaml.safe_load(path.read_text())
        metadata_keys_to_keep = ('labels', 'name')
        metadata = {k: v for k, v in configmap.get('metadata', {}).items()
                    if k in metadata_keys_to_keep}
        first_name, first_config = next(iter(configmap['data'].items()))
        current = {'.metadata': metadata, '.filename': first_name}
        current.update(RunnerConfig._load_config(first_config))
        return current

    def _load_configfile(self, filename):
        """Load a gitlab-runner config file from a file if possible."""
        path = pathlib.Path(self.args.directory, filename)
        if not path.exists():
            return {}
        return RunnerConfig._load_config(path.read_text())

    @staticmethod
    def _dump_config(config):
        """Dump a gitlab-runner config file to a string."""
        config = pt_utils.clean_config(config)
        config = RunnerConfig._sorted(config)
        config['runners'] = sorted(config.get('runners', []),
                                   key=lambda r: r['name'])
        return toml.dumps(config)

    @staticmethod
    def _dump_configmap(config):
        """Dump a gitlab-runner config file to a ConfigMap."""
        return RunnerConfig._yaml_dump({
            'apiVersion': 'v1',
            'kind': 'ConfigMap',
            'metadata': config.get('.metadata', {}),
            'data': {
                config.get('.filename'): RunnerConfig._dump_config(config)
            }
        }, sort_keys=False)

    def _dump_configurations(self):
        """Perform a 'configurations dump' command."""
        current = {}
        for deployment_config in self.runner_deployments.values():
            configmap = deployment_config.get('.configmap')
            configfile = deployment_config.get('.configfile')
            if configmap:
                current[configmap] = self._dump_configmap(
                    self._load_configmap(configmap))
            else:
                current[configfile] = self._dump_config(
                    self._load_configfile(configfile))
        return self._sorted(current)

    def _generate_configurations(self):
        """Perform a 'configurations generate' command."""
        proposed = {}
        runners = self._configurations_runners(self.runner_configurations)
        for name, deployment_config in self.runner_deployments.items():
            deployment_config = copy.deepcopy(deployment_config)
            deployment_config['runners'] = sorted(
                (r for r in runners if r['.deployment_name'] == name),
                key=lambda r: r['name'])
            configmap = deployment_config.get('.configmap')
            configfile = deployment_config.get('.configfile')
            if configmap:
                proposed[configmap] = self._dump_configmap(deployment_config)
            else:
                proposed[configfile] = self._dump_config(deployment_config)
        return self._sorted(proposed)

    def _apply_configurations(self):
        """Perform a 'configurations apply' command."""
        current = self._dump_configurations()
        proposed = self._generate_configurations()

        if current == proposed:
            print(f'configurations: nothing to do', file=sys.stderr)
            return

        if self.token_not_found and not self.args.dry_run:
            raise Exception('Cannot apply configurations with missing tokens')

        for name, config in proposed.items():
            print(f'{name}: updating configuration', file=sys.stderr)
            if not self.args.dry_run:
                pathlib.Path(self.args.directory, name).write_text(config)

    def _dump_registrations(self, clean_ids=True):
        """Perform a 'registrations dump' command."""
        proposed_runners = self._generate_registrations().keys()
        groups = set()
        projects = set()
        for registration in self.runner_registrations:
            groups.update(self._list(registration, 'gitlab_groups'))
            projects.update(self._list(registration, 'gitlab_projects'))

        result = self._sorted({name: runner
                               for name, runner in self._gitlab_runners.items()
                               if name in proposed_runners or
                               not groups.isdisjoint(runner['.groups']) or
                               not projects.isdisjoint(runner['.projects'])})
        if clean_ids:
            for name, runner in result.items():
                runner.pop('.id', None)
        return result

    def _generate_registrations(self):
        """Perform a 'registrations generate' command."""
        proposed = {}
        for registration in self.runner_registrations:
            configuration_names = self._list(
                registration, 'runner_configurations')
            groups = self._list(registration, 'gitlab_groups')
            projects = self._list(registration, 'gitlab_projects')
            for runner in self._configurations_runners(configuration_names):
                proposed[runner['name']] = {
                    '.groups': groups,
                    '.projects': projects,
                    'description': runner['name'],
                    'access_level': runner.get('.access_level',
                                               'not_protected'),
                    'active': runner.get('.active', True),
                    'locked': runner.get('.locked', True),
                    'maximum_timeout': runner.get('.maximum_timeout',
                                                  24 * 60 * 60 * 7),
                    'run_untagged': runner.get('.run_untagged', False),
                    'tag_list': sorted(self._list(runner.get('.tags', {}),
                                                  self.args.tag_assignment)),
                }
        return self._sorted(proposed)

    def _apply_registration(self, name, current_registration,
                            proposed_registration):
        """Perform part of a 'registrations apply' command."""
        groups = proposed_registration['.groups']
        projects = proposed_registration['.projects']
        if not current_registration:
            if not self.args.create_missing:
                raise Exception(f'Missing runner registration {name} not '
                                'created without --create-missing')
            registration_data = self._registration_data(groups + projects)
            if not registration_data:
                raise Exception(f'No registration token found for {name}')

            print(f'{name}: creating registration {name}', file=sys.stderr)
            if not self.args.dry_run:
                proposed_payload = pt_utils.clean_config(proposed_registration)
                proposed_payload['token'] = registration_data['token']
                instance = registration_data['instance']
                gl_instance = self._gl_instance(instance)
                runner = gl_instance.runners.create(proposed_payload)
                print(f'NEW RUNNER AUTH TOKEN: {name}: {runner.token}')
                current_registration = self._gitlab_runner(instance, runner.id)
            else:
                # current_registration cannot be created
                return
        else:
            current_payload = pt_utils.clean_config(current_registration)
            proposed_payload = pt_utils.clean_config(proposed_registration)
            if current_payload != proposed_payload:
                print(f'{name}: updating registration {name}', file=sys.stderr)
                if not self.args.dry_run:
                    instance = self._get_instance_name(name, groups + projects)
                    gl_instance = self._gl_instance(instance)
                    runner = gl_instance.runners.update(
                        current_registration['.id'], proposed_payload)

        if groups != current_registration['.groups']:
            raise Exception(f'Cannot change group for {name}')
        for project in set(projects) - set(current_registration['.projects']):
            print(f'{name}: enabling project {project}', file=sys.stderr)
            if not self.args.dry_run:
                instance = self._get_instance_name(name, [project])
                gl_instance = self._gl_instance(instance)
                if proposed_registration['locked']:
                    gl_instance.runners.update(
                        current_registration['.id'], {'locked': False})
                self._gl_project(project).runners.create({
                    'runner_id': current_registration['.id']})
                if proposed_registration['locked']:
                    gl_instance.runners.update(
                        current_registration['.id'], {'locked': True})
        for project in set(current_registration['.projects']) - set(projects):
            print(f'{name}: disabling project {project}', file=sys.stderr)
            if not self.args.dry_run:
                self._gl_project(project).runners.delete(
                    current_registration['.id'])

    def _apply_registrations(self):
        """Perform a 'registrations apply' command."""
        current = self._dump_registrations(clean_ids=False)
        proposed = self._generate_registrations()

        for name, proposed_registration in proposed.items():
            current_registration = current.get(name)
            self._apply_registration(name, current_registration,
                                     proposed_registration)

    def _dump_variables(self):
        """Perform a 'variables dump' command."""
        groups = {}
        projects = {}
        for item in self.gitlab_variables:
            groups.update({g: self._gitlab_variables(self._gl_group(g))
                           for g in self._list(item, 'gitlab_groups')})
            projects.update({p: self._gitlab_variables(self._gl_project(p))
                             for p in self._list(item, 'gitlab_projects')})
        return self._sorted({'groups': groups, 'projects': projects})

    def _generate_variables(self):
        """Perform a 'variables generate' command."""
        projects = {}
        groups = {}
        for item in self.gitlab_variables:
            variables = self.variable_groups[item['variable_group']]
            groups.update({g: copy.deepcopy(variables)
                           for g in self._list(item, 'gitlab_groups')})
            projects.update({p: copy.deepcopy(variables)
                             for p in self._list(item, 'gitlab_projects')})
        return self._sorted({'groups': groups, 'projects': projects})

    def _apply_variable(self, name, gl_project_group,
                        current_variables, proposed_variables):
        """Perform part of a 'variables apply' command."""
        for key in set(current_variables) - set(proposed_variables):
            print(f'{name}: deleting variable {key}', file=sys.stderr)
            if not self.args.dry_run:
                gl_project_group.variables.delete(key)
        for key, value in proposed_variables.items():
            payload = {'key': key, 'value': value,
                       'variable_type': 'env_var'}
            if key not in current_variables:
                print(f'{name}: creating variable {key}', file=sys.stderr)
                if not self.args.dry_run:
                    gl_project_group.variables.create(payload)
            elif current_variables[key] != value:
                print(f'{name}: updating variable {key}', file=sys.stderr)
                if not self.args.dry_run:
                    gl_project_group.variables.update(key, payload)

    def _apply_variables(self):
        """Perform a 'variables apply' command."""
        current = self._dump_variables()
        proposed = self._generate_variables()

        if current == proposed:
            print('variables: nothing to do', file=sys.stderr)
            return

        for name, proposed_variables in proposed['groups'].items():
            gl_project_group = self._gl_group(name)
            current_variables = current['groups'][name]
            self._apply_variable(name, gl_project_group,
                                 current_variables, proposed_variables)
        for name, proposed_variables in proposed['projects'].items():
            gl_project_group = self._gl_project(name)
            current_variables = current['projects'][name]
            self._apply_variable(name, gl_project_group,
                                 current_variables, proposed_variables)

    def dump(self, what):
        """Dump current values."""
        return self._yaml_dump(getattr(self, f'_dump_{what}')())

    def generate(self, what):
        """Generate proposed values."""
        return self._yaml_dump(getattr(self, f'_generate_{what}')())

    def diff(self, what):
        """Diff current and proposed values."""
        current = self.dump(what)
        proposed = self.generate(what)

        if current == proposed:
            print(f'{what}: nothing to do', file=sys.stderr)
            return None

        if self.args.interactive:
            with self._tempfile('current', current) as name1, \
                    self._tempfile('proposed', proposed) as name2:
                subprocess.run(['vimdiff', name1, name2], check=False)
            return None

        return '\n'.join(difflib.unified_diff(
            current.split('\n'), proposed.split('\n'),
            fromfile='current', tofile='proposed', n=self.args.context))

    def apply(self, what):
        """Apply the proposed values."""
        return getattr(self, f'_apply_{what}')()


def main(argv):
    """Command line interface to the gitlab-runner config management."""
    objects = ['registrations', 'configurations', 'variables']
    actions = ['dump', 'generate', 'diff', 'apply']
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '-c', '--config', default='runners.yaml', type=argparse.FileType('r'),
        help='YAML configuration file to use')
    parser.add_argument(
        '--tag-assignment', default='default',
        help='tag assignment group')
    parser.add_argument(
        '--directory', default=os.getcwd(),
        help='directory for relative file names')
    parser.add_argument(
        'object', choices=objects,
        help='configuration object')
    parser.add_argument(
        'action', choices=actions,
        help='configuration action')

    diff_args = parser.add_argument_group('diff action')
    diff_args.add_argument(
        '--context', default=10, type=int,
        help='number of context lines')
    diff_args.add_argument(
        '-i', '--interactive', action='store_true',
        help='show the differences with vimdiff')

    apply_args = parser.add_argument_group('apply action')
    apply_args.add_argument(
        '--dry-run', action='store_true',
        help='Only print the actions that would be taken, do not take them')
    apply_args.add_argument(
        '--create-missing', action='store_true',
        help='Also perform new runner registrations')

    args = parser.parse_args(argv)

    with args.config as config_file:
        config = yaml.safe_load(config_file)
    runner_config = RunnerConfig(config, args)
    result = getattr(runner_config, args.action)(args.object)
    if result:
        print(result)
    return result


if __name__ == '__main__':
    main(sys.argv[1:])
