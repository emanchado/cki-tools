#!/bin/bash

# no pipefail as the exit code should only be determined by the grep
set -eu

# Check the ability to access the OpenShift API
#
# Usage: openshift-api-check.sh OPENSHIFT_SERVER GREP
#
# Required environment variables:
# - OPENSHIFT_KEY
# - OPENSHIFT_PROJECT

curl --insecure --silent --show-error --header "Authorization: Bearer ${OPENSHIFT_KEY}" "$1/api/v1/namespaces/${OPENSHIFT_PROJECT}/pods" | sponge | grep -q "$2"
echo "Connection succeeded"
