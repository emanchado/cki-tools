#!/usr/bin/python3
import argparse
import datetime
import os
import pathlib
import sys

from cki_lib import kubernetes
from crontab import CronTab


def get_last_terminated_pod(pods, name):
    """Return the last terminated pod for schedule {name}."""
    task_pods = [p for p in pods if p.metadata.labels['schedule_job'] == name]
    for pod in sorted(task_pods, key=lambda x: x.metadata.creation_timestamp, reverse=True):
        if pod.status.container_statuses and pod.status.container_statuses[0].state.terminated:
            return pod
    return None


def check_pod_succeeded(k8s_helper, pod):
    """Check if the execution was successful."""
    termination_reason = pod.status.container_statuses[0].state.terminated.reason
    if termination_reason != 'Completed':
        return f'Terminated: {termination_reason}'


def check_time_since_execution(k8s_helper, pod):
    """
    Check that the last execution was before the period configured.

    Using the cron_job schedule, calculate when the next schedule should have been.
    If the expected_next_execution_in is negative, it means that the task should
    have been already executed and we missed the schedule.
    """
    task_name = pod.metadata.labels['schedule_job']
    last_execution = pod.metadata.creation_timestamp

    api = k8s_helper.api_batchv1beta1()
    cron = api.read_namespaced_cron_job(task_name, k8s_helper.namespace)

    expected_next_execution_in = CronTab(cron.spec.schedule).next(now=last_execution)

    if expected_next_execution_in < 0:
        return f'Schedule missed: {expected_next_execution_in * -1} sec ago'


def main(task_labels=None):
    k8s_helper = kubernetes.KubernetesHelper()
    k8s_helper.setup()
    api = k8s_helper.api_corev1()

    all_pods = api.list_namespaced_pod(k8s_helper.namespace).items
    scheduler_pods = []
    scheduler_tasks = set()

    for pod in all_pods:
        if not pod.metadata.name.startswith('schedule-bot'):
            continue

        task_labels = task_labels or {}
        if not all(pod.metadata.labels.get(key) == value for key, value in task_labels.items()):
            continue

        scheduler_tasks.add(pod.metadata.labels['schedule_job'])
        scheduler_pods.append(pod)

    checkers = [check_pod_succeeded, check_time_since_execution]
    errors = []
    for task_name in scheduler_tasks:
        pod = get_last_terminated_pod(scheduler_pods, task_name)
        if not pod:
            continue
        for check in checkers:
            error = check(k8s_helper, pod)
            if error:
                errors.append(f'{task_name} ({error})')
    if errors:
        print(', '.join(errors))
        sys.exit(1)


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--label', help="Only check for schedule jobs with this label.",
                        action='append', default=[])

    return parser.parse_args()


if __name__ == '__main__':
    arguments = parse_args()

    labels = {}
    for label in arguments.label:
        key, value = label.split('=', 1)
        labels[key] = value

    main(labels)
