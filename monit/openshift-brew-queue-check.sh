#!/bin/bash
set -euo pipefail

# Check the queue size from the brew trigger
#
# Usage: openshift-brew-queue-check.sh BREW_NAME MAX_QUEUE_SIZE
#
# Required environment variables:
# - OPENSHIFT_KEY
# - OPENSHIFT_SERVER
# - OPENSHIFT_PROJECT

# Get the last logged queue size
LAST_QUEUE_SIZE="$(curl -k -s -H "Authorization: Bearer ${OPENSHIFT_KEY}" "${OPENSHIFT_SERVER}/apis/apps.openshift.io/v1/namespaces/${OPENSHIFT_PROJECT}/deploymentconfigs/$1/log?sinceSeconds=120" | sed -En 's/.*queue_size=([0-9]+).*/\1/p' | tail -n 1)"

echo "queue_size=$LAST_QUEUE_SIZE"

test "$LAST_QUEUE_SIZE" -le "$2"
