#!/bin/bash
set -euo pipefail

# Check the status of a system in Beaker
#
# Usage: beaker-system-check.sh SYSTEM_NAME
#
# Required environment variables:
# - BEAKER_URL

SYSTEM_JSON="$(curl -kLs --retry 3 "${BEAKER_URL}/systems/$1/")"
STATUS="$(jq -r .status <<< "${SYSTEM_JSON}")"
RECIPE_ID="$(jq -r .current_reservation.recipe_id <<< "${SYSTEM_JSON}")"

# The recipe_id will be null if the machine isn't doing anything.
if [[ "${RECIPE_ID}" == "null" ]]; then
    STATUS="${STATUS} (idle)"
else
    STATUS="${STATUS} (recipe ${RECIPE_ID})"
fi
echo "${STATUS}"

[[ $STATUS =~ ^Automated ]]
