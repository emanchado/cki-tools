#!/usr/bin/python3
"""
Check with the Data Warehouse whether all pipelines have their reports.

Required environment variables:
- DATAWAREHOUSE_URL
"""
import datetime
import os
import sys

import requests

DATAWAREHOUSE_URL = os.environ['DATAWAREHOUSE_URL']
SESSION = requests.Session()
SESSION.headers.update({'User-Agent': 'reports-checker/monit'})


def main():
    """Check whether all pipelines have their reports."""
    yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
    url = f'{DATAWAREHOUSE_URL}/api/1/report/missing?since={yesterday}'
    response = SESSION.get(url).json()

    pipelines = response['results']['pipelines']

    if not pipelines:
        print(f'📨 All pipelines have their reports!')
        return 0

    missing = ', '.join(f'{p["project"]["path"]}#{p["pipeline_id"]}'
                        for p in pipelines)
    print(f'🔥 {len(pipelines)} pipelines missing report: {missing}')
    return 1


sys.exit(main())
